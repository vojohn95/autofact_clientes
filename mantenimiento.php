<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/logo/favicon/mstile-144x144.png" type="image/x-icon">
    <title>Mantenimiento</title>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merienda+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="construction/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="construction/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="construction/css/animate.css">
    <link rel="stylesheet" type="text/css" href="construction/css/custom.css">

</head>
<body onload="countdown(year,month,day,hour,minute)">
<div class="body-wrapper">

    <!--   begin of header section   -->
    <section class="header" id="overlay-1">
        <div class="header-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="header-text">
                            <h5 class="service-text">¡Este sitio se encuentra en mantenimiento!</h5>
                            <p class="service-text">Estimado usuario , el sitio se encontrará en mantenimiento del 11 al
                                13 del Septiembre , disculpe las molestias esto lo hacemos con el objetivo , de seguirle
                                ofreciendo un servicio de calidad. El 14 de Septiembre , estaremos en linea nuevamente.
                                Muchas gracias por su comprensión.</p>
                        </div>
                    </div>
                </div>

            </div> <!--  end of .container  -->
        </div><!-- end of .header-wrapper  -->
    </section>

    <section class="footer fixed-bottom">
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="copy-right text-center">© <?php echo date("Y"); ?> Copyright</p>
                    </div>
                    <div class="col-md-6">
                        <p class="develop text-center"><a>Operadora Central de Estacionamientos SAPI de C.V. </a></p>
                    </div>
                </div>
            </div>
        </div><!--  end of .footer-wrapper  -->
    </section>
    <!--   end of footer section  -->
</div>

<!--  js files -->

<script type="text/javascript" src="construction/js/jquery.js"></script>
<script type="text/javascript" src="construction/js/bootstrap.min.js"></script>
<script type="text/javascript" src="construction/js/timer.js"></script>
<script type="text/javascript" src="construction/js/script.js"></script>

</body>
</html>