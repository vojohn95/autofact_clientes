<?php
include '../conexion/conexion.php';
/*echo "1";
echo $pass1;
echo $pass2;
echo $id;*/
?>
<!DOCTYPE html>
<html lang="es" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.css">


    <title>Facturacion OCE</title>
</head>
<body>
<?php
$pass1 = htmlentities(trim($_POST['contrasena']));
$pass2 = htmlentities(trim($_POST['rcontrasena']));
$id = htmlentities(trim($_POST['id']));
$id_user = htmlentities(trim($_POST['id_user']));
/*echo "1";
echo $pass1;
echo $pass2;
echo $id;*/
if($pass1 == $pass2){
    //echo "2";
    $contra = strlen($pass1);
    $pass = password_hash($pass1, PASSWORD_DEFAULT, [15]);
    $up= "UPDATE users SET password = '".$pass."' , remember_token='' WHERE id =".$id_user ;
    //echo "---> ".$up." <----";
    //consulta
    if(mysqli_query($mysqli,$up)){
        //echo "4";
        $tituloSWAL = "Exito";
        $mensajeSWAL = "Se cambio la contraseña";
        $t = 'success';
        $dir = '../index';
        //echo "5";
    }else{
        $tituloSWAL = "Falla en el cambio";
        $mensajeSWAL = "No se pudo cambiar la contraseña";
        $t = 'error';
        $dir = '../index';
        //echo "6";
    }
}else{
    $tituloSWAL = "las contraseñas no conciden";
    $mensajeSWAL = "solicita nuevamente tu contraseña";
    $t = 'error';
    $dir = '../index';
    //echo "7";
}
//echo "------> ".$tituloSWAL;
?>

<script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
<script>
    swal({
        title: '<?php echo $tituloSWAL ?>',
        text: '<?php echo $mensajeSWAL ?>',
        type: '<?php echo $t ?>',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'ok'
    }).then(function(){
        location.href='<?php echo $dir ?>';
    });

    $(document).click(function(){
        location.href='<?php echo $dir ?>';
    });

    $(document).keyup(function(e){
        if (e.which == 27) {
            location.href='<?php echo $dir ?>';
        }
    });

</script>

</body>
</html>
