<?php
include '../extend/header.php';
//var_dump($_SESSION);
$ID = $_SESSION['id'];
$correo = $_SESSION['mail'];

?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">


            <!--Accordion wrapper-->
            <div class="accordion md-accordion accordion-blocks" id="accordionEx78" role="tablist"
                 aria-multiselectable="true">

                <!-- Accordion card -->
                <div class="card">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="headingUnfiled">

                        <!--Options-->
                        <div class="dropdown float-left">
                            <i class="fas fa-book"></i>
                        </div>

                        <!-- Heading -->
                        <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapseUnfiled"
                           aria-expanded="true"
                           aria-controls="collapseUnfiled">
                            <h5 class="mt-1 mb-0 green-text">
                                <span>&nbspMis datos fiscales</span>
                                <i class="fas fa-angle-down rotate-icon"></i>
                            </h5>
                        </a>

                    </div>

                    <!-- Card body -->
                    <div id="collapseUnfiled" class="collapse" role="tabpanel" aria-labelledby="headingUnfiled"
                         data-parent="#accordionEx78">
                        <div class="card-body">
                            <?php
                            //Comienza consulta sobre la informacion del cliente
                            $con = "SELECT * FROM user_infos INNER JOIN users ON user_infos.id_cliente = users.id WHERE user_infos.id_cliente = '" . $ID . "'";
                            $consulta = mysqli_query($mysqli, $con) or die('Error al buscar en la base de datos.');
                            $row = mysqli_num_rows($consulta);
                            while ($f = mysqli_fetch_assoc($consulta)) {
                                $calle = $f['calle'];
                                $no_ext = $f['no_ext'];
                                $no_int = $f['no_int'];
                                $colonia = $f['colonia'];
                                $municipio = $f['municipio'];
                                $estado = $f['estado'];
                                $pais = $f['pais'];
                                $cp = $f['cp'];
                                $rfc = $f['RFC'];
                                ?>
                                <!-- Table responsive wrapper -->
                                <div class="table-responsive mx-3">
                                    <!--Table-->
                                    <table class="table table-hover mb-0">

                                        <thead>
                                        <tr>
                                            <th class="th-lg"><a>Calle</a></th>
                                            <th class="th-lg"><a>No. exterior</a></th>
                                            <th class="th-lg"><a>No. interior</a></th>
                                            <th class="th-lg"><a>Colonia</a></th>
                                            <th class="th-lg"><a>Municipio</a></th>
                                            <th class="th-lg"><a>Estado</a></th>
                                            <th class="th-lg"><a>Código Postal</a></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?php echo $calle; ?></td>
                                            <td><?php echo $no_ext; ?></td>
                                            <td><?php echo $no_int; ?></td>
                                            <td><?php echo $colonia; ?></td>
                                            <td><?php echo $municipio; ?></td>
                                            <td><?php echo $estado; ?></td>
                                            <td><?php echo $cp; ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!--Table-->

                                </div>
                            <?php } ?>
                            <!-- Table responsive wrapper -->
                        </div>
                    </div>
                </div>
                <!-- Accordion card -->

                <!-- Accordion card -->
                <div class="card">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="heading79">

                        <!--Options-->
                        <div class="dropdown float-left">
                            <i class="fas fa-pencil-alt"></i>
                        </div>

                        <!-- Heading -->
                        <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse79" aria-expanded="true"
                           aria-controls="collapse79">
                            <h5 class="mt-1 mb-0 green-text">
                                <span> &nbspPara un mejor control, te invitamos a agregar tu dirección fiscal</span>
                                <i class="fas fa-angle-down rotate-icon"></i>
                            </h5>
                        </a>

                    </div>

                    <!-- Card body -->
                    <div id="collapse79" class="collapse" role="tabpanel" aria-labelledby="heading79"
                         data-parent="#accordionEx78">
                        <div class="card-body">

                            <!--Top Table UI-->
                            <div class="table-ui p-2 mb-3 mx-3 mb-4">

                                <!--Grid row-->
                                <div class="row">
                                    <form method="post" action="agregafiscal"
                                          enctype="application/x-www-form-urlencoded">
                                        <div class="form-row">
                                            <!-- Grid column -->
                                            <div class="col-3">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="text" id="calle" name="calle"
                                                           class="form-control validate" required>
                                                    <input type="hidden" id="rfc" name="rfc" value="<?php echo $rfc ?>">
                                                    <label for="callemanual" data-error="Incorrecto"
                                                           data-success="Correcto">Calle</label>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="text" id="noext" name="noext"
                                                           class="form-control validate" required>
                                                    <label for="noext" data-error="Ingrese solo letras y numeros"
                                                           data-success="Correcto">No. Exterior</label>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="text" id="noint" name="noint"
                                                           class="form-control validate" required>
                                                    <label for="noint" data-error="Ingrese solo letras y numeros"
                                                           data-success="Correcto">No. Interior</label>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="text" id="colonia" name="colonia"
                                                           class="form-control validate" required>
                                                    <label for="colonia" data-error="Incorrecto"
                                                           data-success="Correcto">Colonia</label>
                                                </div>
                                            </div>

                                            <!--<div class="col-3">

                                                <div class="md-form md-outline">
                                                    <input type="text" id="pais" class="form-control validate">
                                                    <label for="pais" data-error="Incorrecto" data-success="Correcto">Pais</label>
                                                </div>
                                            </div>-->
                                            <div class="col-3">
                                                <!-- Material input -->
                                                <select class="mdb-select colorful-select dropdown-default md-form"
                                                        searchable="Buscar aquí.." id="selectestado" name="selectestado"
                                                        required>
                                                    <option value="">Elija un estado</option>
                                                    <option value="Aguascalientes">Aguascalientes</option>
                                                    <option value="Baja California">Baja California</option>
                                                    <option value="Baja California Sur">Baja California Sur</option>
                                                    <option value="Campeche">Campeche</option>
                                                    <option value="Coahuila de Zaragoza">Coahuila de Zaragoza</option>
                                                    <option value="Colima">Colima</option>
                                                    <option value="Chiapas">Chiapas</option>
                                                    <option value="Chihuahua">Chihuahua</option>
                                                    <option value="Ciudad de México">Ciudad de México</option>
                                                    <option value="Durango">Durango</option>
                                                    <option value="Guanajuato">Guanajuato</option>
                                                    <option value="Guerrero">Guerrero</option>
                                                    <option value="Hidalgo">Hidalgo</option>
                                                    <option value="Jalisco">Jalisco</option>
                                                    <option value="México">México</option>
                                                    <option value="Michoacán de Ocampo">Michoacán de Ocampo</option>
                                                    <option value="Morelos">Morelos</option>
                                                    <option value="Nayarit">Nayarit</option>
                                                    <option value="Nuevo León">Nuevo León</option>
                                                    <option value="Oaxaca">Oaxaca</option>
                                                    <option value="Puebla">Puebla</option>
                                                    <option value="Querétaro">Querétaro</option>
                                                    <option value="Quintana Roo">Quintana Roo</option>
                                                    <option value="San Luis Potosí">San Luis Potosí</option>
                                                    <option value="Sinaloa">Sinaloa</option>
                                                    <option value="Sonora">Sonora</option>
                                                    <option value="Tabasco">Tabasco</option>
                                                    <option value="Tamaulipas">Tamaulipas</option>
                                                    <option value="Tlaxcala">Tlaxcala</option>
                                                    <option value="Veracruz de Ignacio de la Llave">Veracruz de Ignacio
                                                        de la
                                                        Llave
                                                    </option>
                                                    <option value="Yucatán">Yucatán</option>
                                                    <option value="Zacatecas">Zacatecas</option>
                                                </select>

                                            </div>
                                            <div class="col-3">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="text" id="municipio" name="municipio"
                                                           class="form-control validate" required>
                                                    <label for="municipio" data-error="Ingrese solo letras y numeros"
                                                           data-success="Correcto">Municipio/Delegación </label>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="text" id="ciudad" name="ciudad"
                                                           class="form-control validate" required>
                                                    <label for="ciudad" data-error="Incorrecto"
                                                           data-success="Correcto">Ciudad</label>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="number" id="codigopostal" min="1" pattern="^[0-9]+"
                                                           name="codigopostal" class="form-control validate" required>
                                                    <label for="codigopostal" data-error="Incorrecto"
                                                           data-success="Correcto">Código
                                                        postal</label>
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary">Actualizar mis datos</button>


                                    </form>
                                    <!-- termina row -->
                                </div>

                            </div>
                            <!--Grid column-->

                        </div>
                        <!--Grid row-->

                    </div>
                    <!-- Table responsive wrapper -->

                </div>
                <!-- Accordion card -->

            </div>

            <!-- Accordion card -->
            <div class="card">

                <!-- Card header -->
                <div class="card-header" role="tab" id="heading80">
                    <!--Options-->
                    <div class="dropdown float-left">
                        <i class="fas fa-exclamation"></i>
                    </div>

                    <!-- Heading -->
                    <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse80" aria-expanded="true"
                       aria-controls="collapse80">
                        <h5 class="mt-1 mb-0 green-text">
                            <span>&nbspGenerar factura</span>
                            <i class="fas fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>

                <!-- Card body -->
                <div id="collapse80" class="collapse show" role="tabpanel" aria-labelledby="heading80"
                     data-parent="#accordionEx78">
                    <div class="card-body">

                        <?php
                        //Comienza consulta sobre la informacion del cliente
                        $con_generar = "SELECT * FROM user_infos WHERE RFC = '" . $rfc . "'";
                        $consulta_generar = mysqli_query($mysqli, $con_generar) or die('Error al buscar en la base de datos.');
                        $row_generar = mysqli_num_rows($consulta_generar);
                        while ($fg = mysqli_fetch_assoc($consulta_generar)) {
                            $calleg = $fg['calle'];
                            $no_extg = $fg['no_ext'];
                            $no_intg = $fg['no_int'];
                            $coloniag = $fg['colonia'];
                            $municipiog = $fg['municipio'];
                            $estadog = $fg['estado'];
                            $cpg = $fg['cp'];
                            $rfcg = $fg['RFC'];
                        }

                        $razon = $_SESSION['name'];
                        ?>
                            <div class="row">
                                <!-- Grid column -->
                                <div class="col">
                                    <form method="post" action="estacionamientos" enctype="multipart/form-data">
                                        <div class="md-form">
                                            <i class="fas fa-car prefix"></i>
                                            <input type="number" id="numeroest" name="numeroest"
                                                   class="form-control validate" min="1" pattern="^[0-9]+" required>
                                            <input type="hidden" id="razonsocial" name="razonsocial"
                                                   value="<?= $razon ?>">
                                            <input type="hidden" id="calle" name="calle" value="<?= $calleg; ?>">
                                            <input type="hidden" id="no_ext" name="no_ext" value="<?= $no_extg; ?>">
                                            <input type="hidden" id="no_int" name="no_int" value="<?= $no_intg; ?>">
                                            <input type="hidden" id="colonia" name="colonia"
                                                   value="<?= $coloniag; ?>">
                                            <input type="hidden" id="municipio" name="municipio"
                                                   value="<?= $municipiog; ?>">
                                            <input type="hidden" id="estado" name="estado" value="<?= $estadog; ?>">
                                            <input type="hidden" id="cp" name="cp" value="<?= $cpg; ?>">
                                            <input type="hidden" id="rfc" name="rfc" value="<?= $rfcg; ?>">
                                            <input type="hidden" id="correo" name="correo" value="<?= $correo; ?>">
                                            <label for="numeroest" data-error="Ingrese un numero"
                                                   data-success="Correcto">Número de estacionamiento</label>
                                        </div>
                                </div>
                                <div class="col">
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-sm">Continuar</button>
                                </div>
                            </div>
                        </form>
                        <?php
                        $datos = [
                                "razon" => $razon,
                                "calle" => $calleg,
                                "no_ext" => $no_extg,
                                "no_int" => $no_intg,
                                "colonia" => $coloniag,
                                "municipio" => $municipiog,
                                "estado" => $estadog,
                                "cp" => $cpg,
                                "rfc" => $rfcg,
                                "correo" => $correo
                        ];

                        $datos = serialize($datos);
                        $datos = base64_encode($datos);
                        $datos = urlencode($datos);
                        ?>
                        <span>Si no conoces el estacionamiento</span><a href='estacionamientos?datos=<?=$datos;?>&est=1'> pulsa aquí</a>
                        <!--<span>Si no conoces el estacionamiento</span><a href='estacionamientos?est=1&razon=<?=$razon;?>&calle=<?=$calleg?>&no_ext=<?=$no_extg?>'> pulsa aquí</a>-->
                </div>
            </div>
        </div>
        <!-- Accordion card -->

        <!-- Accordion card -->
        <div class="card">

            <!-- Card header -->
            <div class="card-header" role="tab" id="heading79">

                <!--Options-->
                <div class="dropdown float-left">
                    <i class="fas fa-dollar-sign"></i>
                </div>

                <!-- Heading -->
                <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse90" aria-expanded="true"
                   aria-controls="collapse79">
                    <h5 class="mt-1 mb-0 green-text">
                        <span> &nbspMis facturas</span>
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </h5>
                </a>

            </div>

            <!-- Card body -->
            <div id="collapse90" class="collapse" role="tabpanel" aria-labelledby="heading79"
                 data-parent="#accordionEx78">
                <div class="card-body">

                    <!--Top Table UI-->
                    <div class="table-ui p-2 mb-3 mx-3 mb-4">

                        <!--Grid row-->
                        <div class="row">
                            <div class="table-responsive text-nowrap">
                                <table class="table" id="dtBasicExample">
                                    <thead>
                                    <tr>
                                        <th scope="col">Factura</th>
                                        <th scope="col">Importe</th>
                                        <th scope="col">Fecha</th>
                                        <th scope="col">Estatus</th>
                                    </tr>
                                    </thead>
                                    <?php
                                    //Comienza consulta sobre la informacion del cliente
                                    //$con_fact = "SELECT factura, total_ticket, fecha_emision, estatus FROM autof_tickets WHERE RFC = '" . $rfc . "' AND email = '" . $correo . "'";
                                    $con_fact = "SELECT af.id_ticketAF, at.factura , at.total_ticket, at.fecha_emision, at.estatus FROM autof_tickets at, auto_facts af WHERE at.id = af.id_ticketAF and  RFC = '" . $rfc . "' AND email = '" . $correo . "'";
                                    $consulta_fact = mysqli_query($mysqli, $con_fact) or die('Error al buscar en la base de datos.');
                                    $row_fact = mysqli_num_rows($consulta_fact);
                                    while ($f = mysqli_fetch_assoc($consulta_fact)) {
                                        $i = 1;
                                        $factura = $f['factura'];
                                        $total = $f['total_ticket'];
                                        $fecha_emision = $f['fecha_emision'];
                                        $estatus = $f['estatus'];
                                        ?>
                                        <tbody>
                                        <tr>
                                            <td><?php echo $factura; ?></td>
                                            <td><?php echo $total; ?></td>
                                            <td><?php echo $fecha_emision; ?></td>
                                            <td><?php
                                                if ($estatus == "validar") {
                                                    echo "<h5 class='blue-text'>Pendiente</h5>";
                                                } elseif ($estatus == "valido") {
                                                    echo "<h5><a class='green-text' href='descarga?fact=$factura&rfc=$rfc'>Facturado</a></h5>";
                                                } else {
                                                    echo "<h5 class='red-text'>Rechazado</h5>";
                                                }
                                                ?></td>
                                        </tr>
                                        </tbody>
                                        <?php
                                        $i++;
                                    } ?>
                                </table>
                            </div>

                            <!-- termina row -->
                        </div>

                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

            </div>
            <!-- Table responsive wrapper -->

        </div>
        <!-- Accordion card -->


    </div>
    <!--/.Accordion wrapper-->


</div>
</div>
</div>

</main>
</body>
<p>Nota: Puede reenviar su factura solo dando click sobre su estatus "Facturado"</p>

<br>
<?php include '../extend/footer.php'; ?>
<script src="../js/pagination.js"></script>

