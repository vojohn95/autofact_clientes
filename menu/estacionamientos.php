<?php
include '../conexion/conexion.php';

if ($_GET['est'] == 1) {
    /* Deshacemos el trabajo hecho por base64_encode */
    $error = base64_decode($_GET['datos']);
    /* Deshacemos el trabajo hecho por 'serialize' */
    $error = unserialize($error);
    $estacionamiento = $_GET['est'];
    $razon_social = $error['razon'];
    $calle = $error['calle'];
    $no_ext = $error['no_ext'];
    $no_int = $error['no_int'];
    $colonia = $error['colonia'];
    $municipio = $error['municipio'];
    $estado = $error['estado'];
    $cp = $error['cp'];
    $rfc = $error['rfc'];
    $correo = $error['correo'];

    include "../extend/header.php";
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <div class="card">
                    <h5 class="card-header default-color white-text text-center py-4">
                        <strong>Agregue los datos solicitados</strong>
                    </h5>

                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-0">

                        <!-- Form -->
                        <form class="text-center" style="color: #757575;" action="../process/manual" method="post"
                              enctype="multipart/form-data">

                            <div class="form-row">
                                <div class="col">
                                    <!-- First name -->
                                    <div class="md-form">
                                        <select class="mdb-select colorful-select dropdown-default md-form"
                                                searchable="Buscar aquí.." id="selectconceptomanual"
                                                name="selectconceptomanual" required>
                                            <option value="">Elija un Uso CFDI*</option>
                                            <option value="G01">(G01) Adquisición de mercancias</option>
                                            <option value="G02">(G02) Devoluciones, descuentos o bonificaciones
                                            </option>
                                            <option value="G03">(G03) Gastos en general</option>
                                            <option value="I01">(I01) Construcciones</option>
                                            <option value="I02">(I02) Mobiliario y equipo de oficina por
                                                inversiones
                                            </option>
                                            <option value="I03">(I03) Equipo de transporte</option>
                                            <option value="I04">(I04) Equipo de computo y accesorios</option>
                                            <option value="I05">(I05) Dados, troqueles, moldes, matrices y
                                                herramental
                                            </option>
                                            <option value="I06">(I06) Comunicaciones telefónicas</option>
                                            <option value="I07">(I07) Comunicaciones satelitales</option>
                                            <option value="I08">(I08) Otra maquinaria y equipo</option>
                                            <option value="D01">(D01) Honorarios médicos, dentales y gastos
                                                hospitalarios.
                                            </option>
                                            <option value="D02">(D02) Gastos médicos por incapacidad o
                                                discapacidad
                                            </option>
                                            <option value="D03">(D03) Gastos funerales.</option>
                                            <option value="D04">(D04) Donativos.</option>
                                            <option value="D05">(D05) Intereses reales efectivamente pagados por
                                                créditos hipotecarios (casa habitación).
                                            </option>
                                            <option value="D06">(D06) Aportaciones voluntarias al SAR.</option>
                                            <option value="D07">(D07) Primas por seguros de gastos médicos.
                                            </option>
                                            <option value="D08">(D08) Gastos de transportación escolar
                                                obligatoria.
                                            </option>
                                            <option value="D09">(D09) Depósitos en cuentas para el ahorro, primas
                                                que
                                                tengan como base planes de pensiones.
                                            </option>
                                            <option value="D10">(D10) Pagos por servicios educativos
                                                (colegiaturas)
                                            </option>
                                            <option value="P01">(P01) Por definir</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <!-- Last name -->
                                    <div class="md-form">
                                        <select class="mdb-select colorful-select dropdown-default md-form"
                                                searchable="Buscar aquí.." id="selectpagomanual"
                                                name="selectpagomanual" required>
                                            <option value="">Elija su forma de pago*</option>
                                            <option value="01">(01) Efectivo</option>
                                            <option value="02">(02) Cheque nominativo</option>
                                            <option value="03">(03) Transferencia electrónica de fondos</option>
                                            <option value="04">(04) Tarjeta de crédito</option>
                                            <option value="05">(05) Monedero electrónico</option>
                                            <option value="06">(06) Dinero electrónico</option>
                                            <option value="08">(08) Vales de despensa</option>
                                            <option value="28">(28) Tarjeta de débito</option>
                                            <option value="29">(29) Tarjeta de servicio</option>
                                            <option value="99">(99) Otros</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col">
                                    <!-- First name -->
                                    <div class="md-form">
                                        <div class="md-form md-outline">
                                            <input type="number" id="importe" min="1" pattern="^[0-9]+"
                                                   name="importe" class="form-control" step=".01" required>
                                            <label for="importe">Importe</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <!-- Last name -->
                                    <div class="md-form">
                                        <input placeholder="Selecciona una fecha" type="date" id="datepickermanual"
                                               name="datepickermanual"
                                               class="form-control datepicker" required>
                                    </div>
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col">
                                    <!-- First name -->
                                    <label for="fileinputmanual">Selecciona la foto de tu ticket*</label>
                                    <div class="file-field">
                                        <div class="btn btn-default btn-sm float-left">
                                            <span>Seleccionar archivo</span>
                                            <input type="file" name="fileinputmanual" id="fileinputmanual"
                                                   accept="image/*" required>
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" name="fileinputmanual"
                                                   id="fileinputmanual" accept="image/*" type="text"
                                                   placeholder="Cargar archivo" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="md-form md-outline">
                                        <input type="number" min="1" pattern="^[0-9]+" id="Folio" name="Folio"
                                               class="form-control" required>
                                        <label for="Folio">Número boleto</label>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <input type="hidden" id="razonsocial" name="razonsocial" value="<?= $razon_social; ?>">
                    <input type="hidden" id="calle" name="calle" value="<?= $calle; ?>">
                    <input type="hidden" id="no_ext" name="no_ext" value="<?= $no_ext; ?>">
                    <input type="hidden" id="no_int" name="no_int" value="<?= $no_int; ?>">
                    <input type="hidden" id="colonia" name="colonia" value="<?= $colonia; ?>">
                    <input type="hidden" id="municipio" name="municipio" value="<?= $municipio; ?>">
                    <input type="hidden" id="estado" name="estado" value="<?= $estado; ?>">
                    <input type="hidden" id="cp" name="cp" value="<?= $cp; ?>">
                    <input type="hidden" id="rfc" name="rfc" value="<?= $rfc; ?>">
                    <input type="hidden" id="no_est" name="no_est" value="<?= $estacionamiento; ?>">
                    <input type="hidden" id="ncorreo" name="correo" value="<?= $correo; ?>">


                    <hr>

                    <button type="submit" class="btn btn-default">Generar factura</button>
                    <hr>
                    <a href="manual"><i class="fas fa-long-arrow-alt-left"></i> Volver a la ventana anterior</a>
                    </form>
                    <!-- Form -->

                </div>
            </div>
        </div>
    </div>
    </div>
    <br>
    <?php
    include "../extend/footer.php";

} else {

    $numeroest = $_POST['numeroest'];
    $razonsocial = $_POST['razonsocial'];
    $calle = $_POST['calle'];
    $no_ext = $_POST['no_ext'];
    $no_int = $_POST['no_int'];
    $colonia = $_POST['colonia'];
    $municipio = $_POST['municipio'];
    $estado = $_POST['estado'];
    $cp = $_POST['cp'];
    $rfc = $_POST['rfc'];
    $correo = $_POST['correo'];

    //var_dump($numeroest);

    $con = "SELECT Facturable, Automatico FROM parks WHERE no_est = '" . $numeroest . "'";
    $consulta = mysqli_query($mysqli, $con) or die('Error al buscar en la base de datos.');
    $row = mysqli_num_rows($consulta);
    while ($fg = mysqli_fetch_assoc($consulta)) {
        $facturable = $fg['Facturable'];
        $automan = $fg['Automatico'];
    }

    $id_user = "SELECT id FROM users WHERE email = '$correo' ";
    //echo $id_user;
    $consultaid = mysqli_query($mysqli, $id_user);
    $user_id = mysqli_fetch_assoc($consultaid);
    $id = $user_id['id'];

    //var_dump($facturable);
    //var_dump($automan);
    //var_dump($row);

    if ($row == 0) {
        header("Location: solicitud?est=" . $numeroest);
        //header('location:../extend/alerta.php?msj=El numero de estacionamiento no existe&c=menu&p=menu&t=error');
    } else if ($facturable == "1" && $automan == "1") {
        include "../extend/header.php";
        //var_dump($_POST);
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <h5 class="card-header default-color white-text text-center py-4">
                            <strong>Agregue los datos solicitados</strong>
                        </h5>

                        <!--Card content-->
                        <div class="card-body px-lg-5 pt-0">

                            <!-- Form -->
                            <form class="text-center" style="color: #757575;" action="../process/automatico"
                                  method="post" enctype="multipart/form-data">

                                <div class="form-row">
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <select class="mdb-select colorful-select dropdown-default md-form"
                                                    searchable="Buscar aquí.." id="conceptoauto" name="conceptoauto"
                                                    required>
                                                <option value="">Elija un Uso CFDI*</option>
                                                <option value="G01">(G01) Adquisición de mercancias</option>
                                                <option value="G02">(G02) Devoluciones, descuentos o bonificaciones
                                                </option>
                                                <option value="G03">(G03) Gastos en general</option>
                                                <option value="I01">(I01) Construcciones</option>
                                                <option value="I02">(I02) Mobiliario y equipo de oficina por
                                                    inversiones
                                                </option>
                                                <option value="I03">(I03) Equipo de transporte</option>
                                                <option value="I04">(I04) Equipo de computo y accesorios</option>
                                                <option value="I05">(I05) Dados, troqueles, moldes, matrices y
                                                    herramental
                                                </option>
                                                <option value="I06">(I06) Comunicaciones telefónicas</option>
                                                <option value="I07">(I07) Comunicaciones satelitales</option>
                                                <option value="I08">(I08) Otra maquinaria y equipo</option>
                                                <option value="D01">(D01) Honorarios médicos, dentales y gastos
                                                    hospitalarios.
                                                </option>
                                                <option value="D02">(D02) Gastos médicos por incapacidad o
                                                    discapacidad
                                                </option>
                                                <option value="D03">(D03) Gastos funerales.</option>
                                                <option value="D04">(D04) Donativos.</option>
                                                <option value="D05">(D05) Intereses reales efectivamente pagados por
                                                    créditos hipotecarios (casa habitación).
                                                </option>
                                                <option value="D06">(D06) Aportaciones voluntarias al SAR.</option>
                                                <option value="D07">(D07) Primas por seguros de gastos médicos.
                                                </option>
                                                <option value="D08">(D08) Gastos de transportación escolar
                                                    obligatoria.
                                                </option>
                                                <option value="D09">(D09) Depósitos en cuentas para el ahorro, primas
                                                    que
                                                    tengan como base planes de pensiones.
                                                </option>
                                                <option value="D10">(D10) Pagos por servicios educativos
                                                    (colegiaturas)
                                                </option>
                                                <option value="P01">(P01) Por definir</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <select class="mdb-select colorful-select dropdown-default md-form"
                                                    searchable="Buscar aquí.." id="pagoauto" name="pagoauto" required>
                                                <option value="">Elija su forma de pago*</option>
                                                <option value="01">(01) Efectivo</option>
                                                <option value="02">(02) Cheque nominativo</option>
                                                <option value="03">(03) Transferencia electrónica de fondos</option>
                                                <option value="04">(04) Tarjeta de crédito</option>
                                                <option value="05">(05) Monedero electrónico</option>
                                                <option value="06">(06) Dinero electrónico</option>
                                                <option value="08">(08) Vales de despensa</option>
                                                <option value="28">(28) Tarjeta de débito</option>
                                                <option value="29">(29) Tarjeta de servicio</option>
                                                <option value="99">(99) Otros</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <div class="md-form md-outline">
                                                <input type="number" id="importeauto" name="importeauto" min="1"
                                                       pattern="^[0-9]+" class="form-control" step=".01" required>
                                                <label for="importeauto">Importe</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <input placeholder="Selecciona una fecha" type="date" id="dateauto"
                                                   name="dateauto"
                                                   class="form-control datepicker" required>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <div class="md-form md-outline">
                                            <input type="number" id="webid" name="webid" min="1" pattern="^[0-9]+"
                                                   class="form-control" required>
                                            <label for="webid">Número boleto</label>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <input type="hidden" id="razonsocial" name="razonsocial" value="<?= $_SESSION['name']; ?>">
                        <input type="hidden" id="calle" name="calle" value="<?= $calle; ?>">
                        <input type="hidden" id="no_ext" name="no_ext" value="<?= $no_ext; ?>">
                        <input type="hidden" id="no_int" name="no_int" value="<?= $no_int; ?>">
                        <input type="hidden" id="colonia" name="colonia" value="<?= $colonia; ?>">
                        <input type="hidden" id="municipio" name="municipio" value="<?= $municipio; ?>">
                        <input type="hidden" id="estado" name="estado" value="<?= $estado; ?>">
                        <input type="hidden" id="cp" name="cp" value="<?= $cp; ?>">
                        <input type="hidden" id="rfc" name="rfc" value="<?= $rfc; ?>">
                        <input type="hidden" id="no_est" name="no_est" value="<?= $numeroest; ?>">
                        <input type="hidden" id="ncorreo" name="correo" value="<?= $correo; ?>">


                        <hr>

                        <button type="submit" class="btn btn-default">Generar factura</button>
                        <hr>
                        <a href="manual"><i class="fas fa-long-arrow-alt-left"></i> Volver a la ventana anterior</a>
                        </form>
                        <!-- Form -->

                    </div>
                </div>
            </div>
        </div>
        </div>
        <br>
        <?php
        include "../extend/footer.php";
    } else if ($facturable == "1" && $automan == "0") {
        include "../extend/header.php";
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <h5 class="card-header default-color white-text text-center py-4">
                            <strong>Agregue los datos solicitados</strong>
                        </h5>

                        <!--Card content-->
                        <div class="card-body px-lg-5 pt-0">

                            <!-- Form -->
                            <form class="text-center" style="color: #757575;" action="../process/manual" method="post"
                                  enctype="multipart/form-data">

                                <div class="form-row">
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <select class="mdb-select colorful-select dropdown-default md-form"
                                                    searchable="Buscar aquí.." id="selectconceptomanual"
                                                    name="selectconceptomanual" required>
                                                <option value="">Elija un Uso CFDI*</option>
                                                <option value="G01">(G01) Adquisición de mercancias</option>
                                                <option value="G02">(G02) Devoluciones, descuentos o bonificaciones
                                                </option>
                                                <option value="G03">(G03) Gastos en general</option>
                                                <option value="I01">(I01) Construcciones</option>
                                                <option value="I02">(I02) Mobiliario y equipo de oficina por
                                                    inversiones
                                                </option>
                                                <option value="I03">(I03) Equipo de transporte</option>
                                                <option value="I04">(I04) Equipo de computo y accesorios</option>
                                                <option value="I05">(I05) Dados, troqueles, moldes, matrices y
                                                    herramental
                                                </option>
                                                <option value="I06">(I06) Comunicaciones telefónicas</option>
                                                <option value="I07">(I07) Comunicaciones satelitales</option>
                                                <option value="I08">(I08) Otra maquinaria y equipo</option>
                                                <option value="D01">(D01) Honorarios médicos, dentales y gastos
                                                    hospitalarios.
                                                </option>
                                                <option value="D02">(D02) Gastos médicos por incapacidad o
                                                    discapacidad
                                                </option>
                                                <option value="D03">(D03) Gastos funerales.</option>
                                                <option value="D04">(D04) Donativos.</option>
                                                <option value="D05">(D05) Intereses reales efectivamente pagados por
                                                    créditos hipotecarios (casa habitación).
                                                </option>
                                                <option value="D06">(D06) Aportaciones voluntarias al SAR.</option>
                                                <option value="D07">(D07) Primas por seguros de gastos médicos.
                                                </option>
                                                <option value="D08">(D08) Gastos de transportación escolar
                                                    obligatoria.
                                                </option>
                                                <option value="D09">(D09) Depósitos en cuentas para el ahorro, primas
                                                    que
                                                    tengan como base planes de pensiones.
                                                </option>
                                                <option value="D10">(D10) Pagos por servicios educativos
                                                    (colegiaturas)
                                                </option>
                                                <option value="P01">(P01) Por definir</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <select class="mdb-select colorful-select dropdown-default md-form"
                                                    searchable="Buscar aquí.." id="selectpagomanual"
                                                    name="selectpagomanual" required>
                                                <option value="">Elija su forma de pago*</option>
                                                <option value="01">(01) Efectivo</option>
                                                <option value="02">(02) Cheque nominativo</option>
                                                <option value="03">(03) Transferencia electrónica de fondos</option>
                                                <option value="04">(04) Tarjeta de crédito</option>
                                                <option value="05">(05) Monedero electrónico</option>
                                                <option value="06">(06) Dinero electrónico</option>
                                                <option value="08">(08) Vales de despensa</option>
                                                <option value="28">(28) Tarjeta de débito</option>
                                                <option value="29">(29) Tarjeta de servicio</option>
                                                <option value="99">(99) Otros</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <div class="md-form md-outline">
                                                <input type="number" id="importe" min="1" pattern="^[0-9]+"
                                                       name="importe" class="form-control" step=".01" required>
                                                <label for="importe">Importe</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <input placeholder="Selecciona una fecha" type="date" id="datepickermanual"
                                                   name="datepickermanual"
                                                   class="form-control datepicker" required>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <!-- First name -->
                                        <label for="fileinputmanual">Selecciona la foto de tu ticket*</label>
                                        <div class="file-field">
                                            <div class="btn btn-default btn-sm float-left">
                                                <span>Seleccionar archivo</span>
                                                <input type="file" name="fileinputmanual" id="fileinputmanual"
                                                       accept="image/*" required>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" name="fileinputmanual"
                                                       id="fileinputmanual" accept="image/*" type="text"
                                                       placeholder="Cargar archivo" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="md-form md-outline">
                                            <input type="number" min="1" pattern="^[0-9]+" id="Folio" name="Folio"
                                                   class="form-control" required>
                                            <label for="Folio">Número boleto</label>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <input type="hidden" id="razonsocial" name="razonsocial" value="<?= $_SESSION['name']; ?>">
                        <input type="hidden" id="calle" name="calle" value="<?= $calle; ?>">
                        <input type="hidden" id="no_ext" name="no_ext" value="<?= $no_ext; ?>">
                        <input type="hidden" id="no_int" name="no_int" value="<?= $no_int; ?>">
                        <input type="hidden" id="colonia" name="colonia" value="<?= $colonia; ?>">
                        <input type="hidden" id="municipio" name="municipio" value="<?= $municipio; ?>">
                        <input type="hidden" id="estado" name="estado" value="<?= $estado; ?>">
                        <input type="hidden" id="cp" name="cp" value="<?= $cp; ?>">
                        <input type="hidden" id="rfc" name="rfc" value="<?= $rfc; ?>">
                        <input type="hidden" id="no_est" name="no_est" value="<?= $numeroest; ?>">
                        <input type="hidden" id="ncorreo" name="correo" value="<?= $correo; ?>">


                        <hr>

                        <button type="submit" class="btn btn-default">Generar factura</button>
                        <hr>
                        <a href="manual"><i class="fas fa-long-arrow-alt-left"></i> Volver a la ventana anterior</a>
                        </form>
                        <!-- Form -->

                    </div>
                </div>
            </div>
        </div>
        </div>
        <br>
        <?php
        include "../extend/footer.php";
    } else if ($facturable == "0") {
        //echo "no es facturable";
        //header ("Location: solicitud.php?est=".$numeroest);
        header('location:../extend/alerta?msj=El estacionamiento no es facturable&c=menu&p=menu&t=error');
    } else {
        header('location:../extend/alerta?msj=Ocurrio un error, intente de nuevo&c=menu&p=menu&t=error');
        //echo "intente de nuevo";
    }
}