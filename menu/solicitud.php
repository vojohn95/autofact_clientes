<?php
$st = $_GET['est'];
?>
<div class="loader"></div>
<style>.loader {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../img/dash.gif') 50% 50% no-repeat #f9f9f9;
        opacity: .8
    }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">$(window).load(function () {
        $(".loader").fadeOut("slow")
    });</script>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Facturación OCE</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/mdb.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
</head>
<style>
    .page-footer {
        margin-top: 0px;
        padding-top: 0px;
    }

</style>

<header>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black">
        <div class="container smooth-scroll">
            <a class="navbar-brand" href="manual"><img src="../img/logo/logo/login-logo.png" style="width: 45px;"
                                                          href="home"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="manual"><i class="fas fa-home"></i>Volver <span class="sr-only">(current)</span></a>
                    </li>


                </ul>
            </div>
        </div>
    </nav>

</header>
<!-- Main Navigation -->
<body>
<main>

    <div class="container">
        <section id="clientes" class="section team-section pb-4 wow fadeIn" data-wow-delay="0.3s">
            <br><br>

            <!-- Section heading -->
            <h2 class="font-weight-bold text-center h1 my-5"> </h2>
            <!-- Section description -->
            <p class="text-center grey-text mb-5 mx-auto w-responsive" style="font-size: 32px;">Por favor llene el formulario mostrado</p>

            <!-- Card -->
            <div class="card">

                <h5 class="card-header aqua-gradient text-center py-4">
                    <!--<strong>Sign up</strong>-->
                </h5>

                <!--Card content-->
                <div class="card-body px-lg-5 pt-0">

                    <!-- Form -->
                    <form class="text-center" style="color: #757575;" action="../process/manForm" method="post" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="col-4">
                                <!-- First name -->
                                <div class="md-form">
                                    <input type="text" id="rfc" name="rfc" minlength="12" maxlength="13" class="form-control" required>
                                    <input type="hidden" id="no_est" name="no_est" value="<?= $st; ?>">
                                    <label for="rfc">RFC*</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="text" id="razonsocial" name="razonsocial" class="form-control" required>
                                    <label for="razonsocial">Razón social*</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="email" id="correo" name="correo" class="form-control" required>
                                    <label for="correo">Correo*</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <!-- First name -->
                                <div class="md-form">
                                    <input type="text" id="calle" name="calle" class="form-control">
                                    <label for="calle">Calle</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="text" id="no_ext" name="no_ext" class="form-control">
                                    <label for="no_ext">Número exterior</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="text" id="no_int" name="no_int" class="form-control">
                                    <label for="no_int">Número interior</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="text" id="colonia" name="colonia" class="form-control">
                                    <label for="colonia">Colonia</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <!-- First name -->
                                <div class="md-form">
                                    <select class="mdb-select colorful-select dropdown-default md-form"
                                            searchable="Buscar aquí.." id="selectestadomanual" name="selectestadomanual">
                                        <option value="">Elija un estado</option>
                                        <option value="Aguascalientes">Aguascalientes</option>
                                        <option value="Baja California">Baja California</option>
                                        <option value="Baja California Sur">Baja California Sur</option>
                                        <option value="Campeche">Campeche</option>
                                        <option value="Coahuila de Zaragoza">Coahuila de Zaragoza</option>
                                        <option value="Colima">Colima</option>
                                        <option value="Chiapas">Chiapas</option>
                                        <option value="Chihuahua">Chihuahua</option>
                                        <option value="Ciudad de México">Ciudad de México</option>
                                        <option value="Durango">Durango</option>
                                        <option value="Guanajuato">Guanajuato</option>
                                        <option value="Guerrero">Guerrero</option>
                                        <option value="Hidalgo">Hidalgo</option>
                                        <option value="Jalisco">Jalisco</option>
                                        <option value="México">México</option>
                                        <option value="Michoacán de Ocampo">Michoacán de Ocampo</option>
                                        <option value="Morelos">Morelos</option>
                                        <option value="Nayarit">Nayarit</option>
                                        <option value="Nuevo León">Nuevo León</option>
                                        <option value="Oaxaca">Oaxaca</option>
                                        <option value="Puebla">Puebla</option>
                                        <option value="Querétaro">Querétaro</option>
                                        <option value="Quintana Roo">Quintana Roo</option>
                                        <option value="San Luis Potosí">San Luis Potosí</option>
                                        <option value="Sinaloa">Sinaloa</option>
                                        <option value="Sonora">Sonora</option>
                                        <option value="Tabasco">Tabasco</option>
                                        <option value="Tamaulipas">Tamaulipas</option>
                                        <option value="Tlaxcala">Tlaxcala</option>
                                        <option value="Veracruz de Ignacio de la Llave">Veracruz de Ignacio de la
                                            Llave
                                        </option>
                                        <option value="Yucatán">Yucatán</option>
                                        <option value="Zacatecas">Zacatecas</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="text" id="municipio" name="municipio" class="form-control">
                                    <label for="municipio">Municipio/Delegación</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="text" id="ciudad" name="ciudad" class="form-control">
                                    <label for="ciudad">Ciudad</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="number" id="codigopostal" name="codigopostal" min="1" pattern="^[0-9]+" class="form-control">
                                    <label for="codigopostal">Código postal</label>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="md-form">
                                    <select class="mdb-select colorful-select dropdown-default md-form"
                                            searchable="Buscar aquí.." id="selectconceptomanual" name="selectconceptomanual" required>
                                        <option value="">Elija un Uso CFDI*</option>
                                        <option value="G01">(G01) Adquisición de mercancias</option>
                                        <option value="G02">(G02) Devoluciones, descuentos o bonificaciones
                                        </option>
                                        <option value="G03">(G03) Gastos en general</option>
                                        <option value="I01">(I01) Construcciones</option>
                                        <option value="I02">(I02) Mobiliario y equipo de oficina por
                                            inversiones
                                        </option>
                                        <option value="I03">(I03) Equipo de transporte</option>
                                        <option value="I04">(I04) Equipo de computo y accesorios</option>
                                        <option value="I05">(I05) Dados, troqueles, moldes, matrices y
                                            herramental
                                        </option>
                                        <option value="I06">(I06) Comunicaciones telefónicas</option>
                                        <option value="I07">(I07) Comunicaciones satelitales</option>
                                        <option value="I08">(I08) Otra maquinaria y equipo</option>
                                        <option value="D01">(D01) Honorarios médicos, dentales y gastos
                                            hospitalarios.
                                        </option>
                                        <option value="D02">(D02) Gastos médicos por incapacidad o
                                            discapacidad
                                        </option>
                                        <option value="D03">(D03) Gastos funerales.</option>
                                        <option value="D04">(D04) Donativos.</option>
                                        <option value="D05">(D05) Intereses reales efectivamente pagados por
                                            créditos hipotecarios (casa habitación).
                                        </option>
                                        <option value="D06">(D06) Aportaciones voluntarias al SAR.</option>
                                        <option value="D07">(D07) Primas por seguros de gastos médicos.
                                        </option>
                                        <option value="D08">(D08) Gastos de transportación escolar
                                            obligatoria.
                                        </option>
                                        <option value="D09">(D09) Depósitos en cuentas para el ahorro, primas
                                            que
                                            tengan como base planes de pensiones.
                                        </option>
                                        <option value="D10">(D10) Pagos por servicios educativos
                                            (colegiaturas)
                                        </option>
                                        <option value="P01">(P01) Por definir</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-4">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input placeholder="Selecciona una fecha*" type="date" id="datepickermanual" name="datepickermanual"
                                           class="form-control datepicker" required>
                                </div>
                            </div>

                            <div class="col-4">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="number" id="importe" name="importe" min="1" max="5000" class="form-control" min="1" pattern="^[0-9]+" required>
                                    <label for="importe">Importe*</label>
                                </div>
                            </div>

                            <div class="col-4">
                                <!-- Last name -->
                                <div class="md-form">
                                    <select class="mdb-select colorful-select dropdown-default md-form"
                                            searchable="Buscar aquí.." id="selectpagomanual" name="selectpagomanual" required>
                                        <option value="">Elija su forma de pago*</option>
                                        <option value="01">(01) Efectivo</option>
                                        <option value="02">(02) Cheque nominativo</option>
                                        <option value="03">(03) Transferencia electrónica de fondos</option>
                                        <option value="04">(04) Tarjeta de crédito</option>
                                        <option value="05">(05) Monedero electrónico</option>
                                        <option value="06">(06) Dinero electrónico</option>
                                        <option value="08">(08) Vales de despensa</option>
                                        <option value="28">(28) Tarjeta de débito</option>
                                        <option value="29">(29) Tarjeta de servicio</option>
                                        <option value="99">(99) Otros</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-4">
                                <!-- Last name -->
                                <div class="md-form">
                                    <input type="number" id="folio" name="folio" class="form-control" min="1" pattern="^[0-9]+" required>
                                    <label for="folio">Número boleto*</label>
                                </div>
                            </div>

                            <div class="col-4">
                                <!-- Last name -->
                                <div class="md-form">

                                    <div class="file-field">
                                        <div class="btn btn-default btn-sm float-left">
                                            <span>Seleccionar archivo*</span>
                                            <input type="file" name="fileinputmanual" id="fileinputmanual" accept="image/*" required>
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" name="fileinputmanual" id="fileinputmanual" accept="image/*" type="text" placeholder="Cargar archivo" required>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <!-- Sign up button -->
                        <button class="btn btn-outline-default btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">Enviar</button>

                        <hr>

                        <!-- Terms of service -->
                        <p>Todos los campos con
                            <em style="color: #0b51c5;">*</em>
                            <em style="color: #0b51c5;">son obligatorios.</em>
                        </p>

                    </form>
                    <!-- Form -->

                </div>




            </div>

        </section>
        <!-- Section: Team v.3 -->
    </div>


    </div>

</main>
<!-- Main Layout -->


<!-- Footer -->
<footer class="page-footer pt-4 mt-4 black text-center text-md-left">

    <!-- Footer Links -->
    <div class="container">
        <div class="row">

            <!-- First column -->
            <div class="col-md-6">
                <!-- Aqui va el certificado-->
                <p></p>
            </div>
            <!-- First column -->
            <div style="padding-left: 250px; padding-top: 50px;">
                <h6><a href="https://www.central-mx.com/aviso-de-privacidad">Aviso de privacidad</a></h6>
            </div>

        </div>
    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright py-3 text-center">
        <div class="container-fluid">
            <img src="../img/logo/logo/login-logo.png" style="width: 60px; padding-right: 10px;"/>
            &copy; 2019 Copyright: <a href="https://www.central-mx.com/"> Operadora Central de Estacionamientos </a>

        </div>
    </div>
    <!-- Copyright -->

</footer>

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<script src="../js/checkbox.js"></script>


</body>

</html>
