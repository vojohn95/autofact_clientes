<?php
include '../conexion/conexion.php';
include "../composer/vendor/autoload.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//var_dump($_GET);
$fact = $_GET['fact'];
$rfc = $_GET['rfc'];

$con = "SELECT id FROM autof_tickets WHERE factura = '" . $fact . "' AND autof_tickets.RFC = '" . $rfc . "' ";
$consulta = mysqli_query($mysqli, $con) or die('Error al buscar en la base de datos.');
$row = mysqli_num_rows($consulta);
while ($f = mysqli_fetch_assoc($consulta)) {
    $ID = $f['id'];
    //var_dump($ID);
}

var_dump("GET: ", $_GET, "ID: ", $ID);

if ($row >= 1) {
    $con1 = "SELECT * FROM autof_tickets INNER JOIN auto_facts ON autof_tickets.id = auto_facts.id_ticketAF WHERE autof_tickets.id = '$ID'";
    $consulta1 = mysqli_query($mysqli, $con1) or die('Error al buscar en la base de datos.');
    //var_dump($consulta1);
    $row1 = mysqli_num_rows($consulta1);

    while ($mostrar = $consulta1->fetch_array()) {
        $xml = $mostrar['XML'];
        $pdf = $mostrar['PDF'];
        $uuid =  $mostrar['uuid'];
        $folioPark =  $mostrar['folio'];
        $seriePark =  $mostrar['serie'];
        $razonsocial =  $mostrar['Razon_social'];
        $email =  $mostrar['email'];
    }


    file_put_contents($uuid . '.xml', $xml);
    $decoded = base64_decode($pdf);
    $file = $uuid . '.pdf';
    file_put_contents($file, $decoded);

    $escrita = $uuid . '.xml';
    $imagen = $uuid . '.pdf';


    $mail = new PHPMailer(true);
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = 'smtp.office365.com';
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = "facturacion-oce@central-mx.com";
    $mail->Password = "1t3gr4d0r2020*";
    $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
    $mail->FromName = "Central operadora de estacionamientos";
    $mail->Subject = "Factura" . $folioPark . $seriePark;
    $mail->Body = "<html><h4 style='font-color: green;' align='center;'>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
 
    <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: $uuid, Serie: $seriePark y Folio: $folioPark. Así como su representación impresa.</p>

<p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>

<p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico $email a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
    $mail->AddAddress($email, $razonsocial);
    $archivo = $uuid . '.xml';
    $pdf = $uuid . '.pdf';
    $mail->AddAttachment($archivo);
    $mail->AddAttachment($pdf);
    $mail->IsHTML(true);
    $mail->Send();
    unlink($archivo);
    unlink($pdf);

    header('location:../extend/alerta?msj=Env\u00edo de factura exitoso&c=menu&p=menu&t=success');
} else {
    header('location:../extend/alerta?msj=No se encontro factura&c=menu&p=menu&t=error');
}
