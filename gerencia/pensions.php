<?php
include '../extend/headergerente.php';
$usuario = $_SESSION['name'];
$consulta = "SELECT user_infos.id, user_infos.Razon_social, user_infos.RFC, pensions.num_pen, pensions.no_est, pensions.created_at FROM pensions INNER JOIN user_infos ON user_infos.id_cliente = pensions.id_cliente WHERE cargado_por = '$usuario'";
$datos= mysqli_query ($mysqli,$consulta);
$row = mysqli_num_rows($datos);
?>
<body>
<div class="container">
    <button type="button" class="btn btn-primary">
        Pensiones encontradas <span class="badge badge-danger ml-4" style="font-size: 12px;"><?= $row; ?></span>
    </button>
    <span class="counter pull-right"></span>
    <br>
    <div class="card">
        <div class="form-group pull-right">
            <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
        </div>
        <table class="table table-hover table-bordered results">
            <thead>
            <tr>
                <th>RFC</th>
                <th>Nombre</th>
                <th># Pension</th>
                <th># Estacionamiento</th>
                <th>Fecha de creación</th>
            </tr>
            <tr class="warning no-result">
                <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontró registro con la información ingresada</td>
            </tr>
            </thead>
            <?php
            while ($fila=mysqli_fetch_array($datos)) {
                $i = 1;
                $fila["user_infos.id"];
                $razon = $fila["Razon_social"];
                $rfc = $fila ["RFC"];
                $num_pen = $fila["num_pen"];
                $no_est = $fila["no_est"];
                $fecha = $fila["created_at"];
                ?>
            <tbody>
                <tr>
                    <th><?php echo $rfc; ?></th>
                    <td><?php echo $razon; ?></td>
                    <td><?php echo $num_pen; ?></td>
                    <td><?php echo $no_est; ?></td>
                    <td><?php echo $fecha; ?></td>
                </tr>
            </tbody>
                <?php
                $i++;
            }
            ?>
        </table>


    </div>

</div>
</body>
<br>
<?php

include '../extend/footergerente.php'; ?>
