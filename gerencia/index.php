<?php
include '../extend/headergerente.php';
$usuario = $_SESSION['name'];
?>

<div class="container">

    <div class="card">

        <form action="process/carga_info" method="post" >

            <h1 class="text-center">¡Bienvenido!</h1>
            <h3 class="text-center">Complete los campos mostrados abajo</h3>
            <br>
        <div class="md-form input-group">
            <input type="number" aria-label="Numero pension" min="1" id="no_pension" name="no_pension" class="form-control" placeholder="Número pensión" required>
            <input type="number" aria-label="Costo/Pensión" class="form-control" id="costo_pen" name="costo_pen" placeholder="Costo / Pensión" min="1" pattern="^[0-9]+" step=".01" required>
            <input type="number" aria-label="Recargos" min="0" class="form-control" id="recargis" name="recargos" placeholder="Recargos" required>
            <input type="number" aria-label="No_estacionamiento" min="1" class="form-control" id="No_estacionamiento" name="No_estacionamiento" placeholder="No. Estacionamiento" required>
            <input type="hidden" id="session" name="session" value="<?= $usuario ?>">
        </div>
            <div class="md-form input-group">
                <input type="number" aria-label="Venta tarjeta" class="form-control" id="vent_tarj" name="vent_tarj" placeholder="Venta tarjeta" min="1" pattern="^[0-9]+" step=".01" required>
                <input type="number" aria-label="Reposición tarjeta" min="0" class="form-control" id="rep_tarjeta" name="rep_tarjeta" placeholder="Reposición tarjeta" min="1" pattern="^[0-9]+" step=".01" required>
                <input type="number" aria-label="Importe de pago" min="1" class="form-control" id="imp_pago" name="imp_pago" placeholder="Importe de pago" min="1" pattern="^[0-9]+" step=".01" required>
            </div>

            <div class="md-form input-group">
                <select class="browser-default custom-select" id="t_pension" name="t_pension" required>
                    <option selected>Tipo pensión</option>
                    <option value="1">Locatario</option>
                    <option value="2">Externo</option>
                </select>
                <select class="browser-default custom-select" id="t_pago" name="t_pago" required>
                    <option selected>Tipo pago</option>
                    <option value="1">Mensual</option>
                    <option value="2">Trimestral</option>
                    <option value="3">Semestral</option>
                    <option value="4">Anual</option>
                </select>
                <select class="browser-default custom-select" id="f_pago" name="f_pago" required>
                    <option selected>Forma pago</option>
                    <option value="1">Deposito</option>
                    <option value="2">Transferencia</option>
                </select>
                <select class="browser-default custom-select" id="mes_pago" name="mes_pago">
                    <option selected>Mes/Pago</option>
                    <option value="1">Enero</option>
                    <option value="2">Febrero</option>
                    <option value="3">Marzo</option>
                    <option value="4">Abril</option>
                    <option value="5">Mayo</option>
                    <option value="6">Junio</option>
                    <option value="7">Julio</option>
                    <option value="8">Agosto</option>
                    <option value="9">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
                </select>
                <select class="browser-default custom-select" id="fecha_lpago" name="fecha_lpago" required>
                    <option selected>Fecha limite de pago</option>
                    <option value="5">dia 5 de cada mes</option>
                    <option value="6">dia 6 de cada mes</option>
                    <option value="7">dia 7 de cada mes</option>
                    <option value="8">dia 8 de cada mes</option>
                    <option value="9">dia 9 de cada mes</option>
                    <option value="10">dia 10 de cada mes</option>
                    <option value="11">dia 11 de cada mes</option>
                    <option value="12">dia 12 de cada mes</option>
                    <option value="13">dia 13 de cada mes</option>
                    <option value="14">dia 14 de cada mes</option>
                    <option value="15">dia 15 de cada mes</option>
                    <option value="16">dia 16 de cada mes</option>
                    <option value="17">dia 17 de cada mes</option>
                    <option value="18">dia 18 de cada mes</option>
                    <option value="19">dia 19 de cada mes</option>
                    <option value="20">dia 20 de cada mes</option>
                    <option value="21">dia 21 de cada mes</option>
                    <option value="22">dia 22 de cada mes</option>
                    <option value="23">dia 23 de cada mes</option>
                    <option value="24">dia 24 de cada mes</option>
                    <option value="25">dia 25 de cada mes</option>
                    <option value="26">dia 26 de cada mes</option>
                    <option value="27">dia 27 de cada mes</option>
                    <option value="28">dia 28 de cada mes</option>
                    <option value="29">dia 29 de cada mes</option>
                    <option value="30">dia 30 de cada mes</option>
                    <option value="31">dia 31 de cada mes</option>
                </select>
                <select class="browser-default custom-select" id="r_factura" name="r_factura">
                    <option selected>Requiere factura</option>
                    <option value="1">Si</option>
                    <option value="0">No</option>
                </select>

            </div>
            <br>
            <h3 class="text-center">Datos de cliente</h3>
            <br>
            <div class="md-form input-group">
                <input type="text" aria-label="Nombre pensionado" id="nom_pensionado" name="nom_pensionado" class="form-control" placeholder="Nombre pensionado" required>
                <input type="text" aria-label="RFC" id="rfc" name="rfc" class="form-control" placeholder="RFC pensionado" required>
                <input type="number" aria-label="Telefono" class="form-control" id="telefono" name="telefono" placeholder="Telefono" min="1" pattern="^[0-9]+" required>
                <input type="email" aria-label="Correo Electronico" lass="form-control" id="email" name="email" placeholder="Correo electronico" required>
                <!--<input type="number" aria-label="Costo pensión" min="1" class="form-control" id="costo_pen" name="costo_pen" placeholder="Costo Pensión">-->
            </div>

            <button type="submit" class="btn btn-default">Subir información</button>

        </form>


    </div>

    </div>
<br>
<?php

include '../extend/footergerente.php'; ?>

