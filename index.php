<?php
include 'conexion/conexion.php';
include 'composer/vendor/autoload.php';
Sentry\init(['dsn' => 'https://da2ed1a4b3c04fd789c70efa6dcc13d9@o514087.ingest.sentry.io/5616842']);

@session_start(); //incluyendo la conexion para sesion
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="autor" content="Luis Fernando Jonathan Vargas Osornio - vojohn95@gmail.com">
    <title>Facturación estacionamiento | Operadora Central de Estacionamientos</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Portal de facturación - Operadora Central de Estacionamientos">
    <meta content="Operadora Central de Estacionamientos facturación" />
    <meta content="Facturación oce" property="og:title">
    <meta content="https://facturacion.central-mx.com" property="og:url">
    <meta content="website" property="og:type">
    <meta name="author" content="Luis Fernando Jonathan Vargas Osornio -vojohn95@gmail.com">
    <meta property="og:locale" content="es_MX">
    <meta property="og:image" content="https://facturacion.central-mx.com/img/logo/Logo.webp">
    <meta property="og:description" content="Portal de facturación - Operadora Central de Estacionamientos">
    <meta property="og:site_name" content="Portal facturacion Operadora central de estacionamientos">
    <meta property="dns-prefetch" href="//fonts.googleapis.com">
    <meta property="dns-prefetch" href="//s.w.org">

    <meta name="Keywords" content="facturacion, factura, oce, central de estacionamiento, operadora central de estacionamientos, copemsa, estacionamientos, facturacion estacionamiento" />
    <meta name="robots" content="index,follow" />
    <meta http-equiv="expires" content="3600" />
    <link rel="icon" href="/img/logo/Logo.webp" />
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MDCHCLS');
    </script>
    <!-- End Google Tag Manager -->
</head>
<style>
    @media (max-width: 740px) {

        html,
        body,
        header,
        .jarallax {
            height: 100vh
        }
    }

    @media (min-width: 800px) and (max-width: 850px) {

        html,
        body,
        header,
        .jarallax {
            height: 100vh
        }
    }

    @media (min-width: 560px) and (max-width: 660px) {
        header .jarallax h5 {
            display: none !important
        }
    }
</style>

<header>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black">
        <div class="container smooth-scroll">
            <a class="navbar-brand" href="#"><img data-src="img/logo/Logo.webp" class="lazyload" style="width: 45px;" href="home" /></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">

                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home"><i class="fas fa-home"></i>Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#clientes" data-offset="60"><i class="fas fa-user-friends"></i>Estacionamientos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#products" data-offset="60"><i class="fas fa-car"></i>Pensiones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact" data-offset="60"><i class="fas fa-phone"></i>Contacto</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="modal" data-target="#modalLRForm"><i class="fas fa-sign-in-alt"></i>Inicio de sesión</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="modal" data-target="#ModalPreguntas"><i class="far fa-question-circle"></i>Preguntas frecuentes</a>
                    </li>
                    <li class="nav-item">
                        <a href="Manual.pdf" target="_blank" class="nav-link"><i class="fas fa-download"></i>Manual de
                            usuario</a>
                        <!--<a class="nav-link"><i class="fas fa-download"></i>Manual de usuario</a>-->
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Intro Section -->
    <div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url(img/background.webp); background-repeat: no-repeat; background-size: cover; background-position: center center;">
        <div class="mask rgba-stylish-light">
            <div class="container h-100 d-flex justify-content-center align-items-center">
                <div class="row pt-5 mt-3">
                    <div class="col-md-12 mb-3">
                        <div class="intro-info-content text-center">
                            <h1 class="display-3 white-text mb-5 wow fadeInDown" data-wow-delay="0.3s" style="font-size: 38px;"><strong>SISTEMA DE FACTURACIÓN</strong></h1>
                            <h5 class="white-text mb-5 mt-1 font-weight-bold wow fadeInDown" data-wow-delay="0.3s">Seleccione la opción a facturar. </h5>
                            <a class="btn btn-lg wow fadeInDown btn-default" data-wow-delay="0.3s" class="nav-link" href="#clientes" data-offset="60">Estacionamientos</a>
                            <a class="btn btn-lg wow fadeInDown btn-default" href="#products" data-wow-delay="0.3s">Pensiones</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
<!-- Main Navigation -->

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MDCHCLS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <main>

        <div class="container">
            <section id="clientes" class="section team-section pb-4 wow fadeIn" data-wow-delay="0.3s">

                <!-- Section heading -->
                <h2 class="font-weight-bold text-center h1 my-5">Facturar</h2>
                <!-- Section description -->
                <p class="text-center grey-text mb-5 mx-auto w-responsive" style="font-size: 32px;">Datos Generales</p>

                <!-- Card -->
                <div class="card hoverable">

                    <div class="card-body">

                        <span>Por favor, digite el número de estacionamiento para facturar</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>En caso de no conocer el número de estacionamiento <a href="solicitud">pulse aquí</a></span>
                        <!-- Empieza formulario -->

                        <div class="row">
                            <div class="col-4">
                                <!-- Material input -->
                                <div class="md-form md-outline">
                                    <!-- <input type="number" id="noestacionamiento" name="noestacionamiento" class="form-control validate" min="1" pattern="^[0-9]+" required> -->
                                    <!-- <label for="noestacionamiento" data-error="Incorrecto" data-success="Correcto">Número
                                        estacionamiento*</label> -->
                                    <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="noestacionamiento" name="noestacionamiento">
                                        <option value="">Elija un estacionamiento</option>
                                        <?php
                                        $selmax = "SELECT no_est, nombre FROM parks where no_est != 1 AND no_est != 1000 AND no_est != 99999 AND no_est != 191919191";
                                        $consultamax = mysqli_query($mysqli, $selmax);
                                        while ($row = mysqli_fetch_assoc($consultamax)) {
                                        ?>
                                            <option value="<?= $row['no_est'] ?>"><?= $row['no_est'] ?> - <?= $row['nombre'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>


                                </div>
                            </div>
                            <div class="col-6 text-left">
                                <br>
                                <button type="button" class="btn btn-sm btn-default btn-rounded" onclick="BuscaEst()">Buscar
                                </button>
                            </div>
                        </div>
                        <div id="loading" style="display: none;">
                            <i class="fa fa-spinner fa-spin"></i>
                        </div>
                        <div id="resultadoBusqueda"></div>
                        <!-- se muestra el formulario de manual-->
                        <div id="manual" style="display:none;">
                            <form id="formManual" enctype="multipart/form-data">
                                <!-- Grid row -->
                                <div class="form-row">
                                    <!-- Grid column -->
                                    <div class="col-6">
                                        <!-- Material input -->
                                        <div class="md-form md-outline">
                                            <input type="text" id="rfcmanual" name="rfcmanual" class="form-control validate" minlength="12" maxlength="13" required>
                                            <input type="hidden" id="NoEstacionamientoManual" name="NoEstacionamientoManual">
                                            <label for="rfcmanual" data-error="Incorrecto" data-success="Correcto">Escribe
                                                tu
                                                RFC*</label>
                                        </div>
                                    </div>
                                    <!-- Grid column -->

                                    <!-- Grid column -->
                                    <div class="col-6">
                                        <!-- Material input -->
                                        <div class="md-form md-outline">
                                            <input type="text" id="razonsocialmanual" name="rasonsocialmanual" class="form-control validate" required>
                                            <label for="razonsocialmanual" data-error="Incorrecto" data-success="Correcto">Razón
                                                social*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="md-form form-check">
                                    <p class="text-center grey-text mb-5 mx-auto w-responsive" style="font-size: 22px;">
                                        Datos
                                        generales</p>
                                    <input type="checkbox" name="checkfisc" class="form-check-input" id="checkfisc">
                                    <label class="form-check-label" for="checkfisc">Pulse aqui para agregar dirección
                                        fiscal</label>
                                </div>
                                <!-- empieza formulario escondido -->
                                <style>
                                    #Check1 {
                                        display: none
                                    }
                                </style>
                                <!-- formulario oculto -->
                                <div id="Check1">
                                    <div class="form-row">
                                        <!-- Grid column -->
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="callemanual" name="callemanual" class="form-control validate">
                                                <label for="callemanual" data-error="Incorrecto" data-success="Correcto">Calle</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="noextmanual" name="noextmanual" class="form-control validate">
                                                <label for="noextmanual" data-error="Ingrese solo letras y numeros" data-success="Correcto">No. Exterior</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="nointmanual" name="nointmanual" class="form-control validate">
                                                <label for="nointmanual" data-error="Ingrese solo letras y numeros" data-success="Correcto">No. Interior</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="coloniamanual" name="coloniamanual" class="form-control validate">
                                                <label for="coloniamanual" data-error="Incorrecto" data-success="Correcto">Colonia</label>
                                            </div>
                                        </div>

                                        <!--<div class="col-3">

                                        <div class="md-form md-outline">
                                            <input type="text" id="pais" class="form-control validate">
                                            <label for="pais" data-error="Incorrecto" data-success="Correcto">Pais</label>
                                        </div>
                                    </div>-->
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="selectestadomanual" name="selectestadomanual">
                                                <option value="">Elija un estado</option>
                                                <option value="Aguascalientes">Aguascalientes</option>
                                                <option value="Baja California">Baja California</option>
                                                <option value="Baja California Sur">Baja California Sur</option>
                                                <option value="Campeche">Campeche</option>
                                                <option value="Coahuila de Zaragoza">Coahuila de Zaragoza</option>
                                                <option value="Colima">Colima</option>
                                                <option value="Chiapas">Chiapas</option>
                                                <option value="Chihuahua">Chihuahua</option>
                                                <option value="Ciudad de México">Ciudad de México</option>
                                                <option value="Durango">Durango</option>
                                                <option value="Guanajuato">Guanajuato</option>
                                                <option value="Guerrero">Guerrero</option>
                                                <option value="Hidalgo">Hidalgo</option>
                                                <option value="Jalisco">Jalisco</option>
                                                <option value="México">México</option>
                                                <option value="Michoacán de Ocampo">Michoacán de Ocampo</option>
                                                <option value="Morelos">Morelos</option>
                                                <option value="Nayarit">Nayarit</option>
                                                <option value="Nuevo León">Nuevo León</option>
                                                <option value="Oaxaca">Oaxaca</option>
                                                <option value="Puebla">Puebla</option>
                                                <option value="Querétaro">Querétaro</option>
                                                <option value="Quintana Roo">Quintana Roo</option>
                                                <option value="San Luis Potosí">San Luis Potosí</option>
                                                <option value="Sinaloa">Sinaloa</option>
                                                <option value="Sonora">Sonora</option>
                                                <option value="Tabasco">Tabasco</option>
                                                <option value="Tamaulipas">Tamaulipas</option>
                                                <option value="Tlaxcala">Tlaxcala</option>
                                                <option value="Veracruz de Ignacio de la Llave">Veracruz de Ignacio de la
                                                    Llave
                                                </option>
                                                <option value="Yucatán">Yucatán</option>
                                                <option value="Zacatecas">Zacatecas</option>
                                            </select>

                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="municipiomanual" name="municipiomanual" class="form-control validate">
                                                <label for="municipiomanual" data-error="Ingrese solo letras y numeros" data-success="Correcto">Municipio/Delegación </label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="ciudadmanual" name="ciudadmanual" class="form-control validate">
                                                <label for="ciudadmanual" data-error="Incorrecto" data-success="Correcto">Ciudad</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="number" id="codigopostalmanual" name="codigopostalmanual" class="form-control validate" min="1" pattern="^[0-9]+">
                                                <label for="codigopostal" data-error="Incorrecto" data-success="Correcto">Código
                                                    postal</label>
                                            </div>
                                        </div>

                                        <!-- termina row -->
                                    </div>

                                </div>

                                <p class="text-center grey-text mb-5 mx-auto w-responsive" style="font-size: 22px;">Uso de
                                    CFDI*</p>
                                <div class="form-row">
                                    <div class="col-3">
                                        <div class="md-form md-outline">
                                            <input type="number" id="codigopostalmanual" name="codigopostalmanual" class="form-control validate" min="1" pattern="^[0-9]+" required>
                                            <label for="codigopostal" data-error="Incorrecto" data-success="Correcto">Código
                                                postal*</label>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="selectconceptomanual" name="selectconceptomanual" required>
                                            <option value="">Elija un Uso CFDI*</option>
                                            <!-- <option value="G01">(G01) Adquisición de mercancias</option>
                                            <option value="G02">(G02) Devoluciones, descuentos o bonificaciones
                                            </option> -->
                                            <option value="G03">(G03) Gastos en general</option>
                                            <option value="S01">(S01) Sin efectos fiscales</option>
                                            <!-- <option value="I01">(I01) Construcciones</option>
                                            <option value="I02">(I02) Mobiliario y equipo de oficina por
                                                inversiones
                                            </option>
                                            <option value="I03">(I03) Equipo de transporte</option>
                                            <option value="I04">(I04) Equipo de computo y accesorios</option>
                                            <option value="I05">(I05) Dados, troqueles, moldes, matrices y
                                                herramental
                                            </option>
                                            <option value="I06">(I06) Comunicaciones telefónicas</option>
                                            <option value="I07">(I07) Comunicaciones satelitales</option>
                                            <option value="I08">(I08) Otra maquinaria y equipo</option>
                                            <option value="D01">(D01) Honorarios médicos, dentales y gastos
                                                hospitalarios.
                                            </option>
                                            <option value="D02">(D02) Gastos médicos por incapacidad o
                                                discapacidad
                                            </option>
                                            <option value="D03">(D03) Gastos funerales.</option>
                                            <option value="D04">(D04) Donativos.</option>
                                            <option value="D05">(D05) Intereses reales efectivamente pagados por
                                                créditos hipotecarios (casa habitación).
                                            </option>
                                            <option value="D06">(D06) Aportaciones voluntarias al SAR.</option>
                                            <option value="D07">(D07) Primas por seguros de gastos médicos.
                                            </option>
                                            <option value="D08">(D08) Gastos de transportación escolar
                                                obligatoria.
                                            </option>
                                            <option value="D09">(D09) Depósitos en cuentas para el ahorro, primas
                                                que
                                                tengan como base planes de pensiones.
                                            </option>
                                            <option value="D10">(D10) Pagos por servicios educativos
                                                (colegiaturas)
                                            </option>
                                            <option value="P01">(P01) Por definir</option> -->
                                        </select>
                                    </div>

                                    <div class="col-4">
                                        <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="regimenfiscal" name="regimenfiscal" required>
                                            <option value="">Elija un Regimen Fiscal*</option>
                                            <?php
                                            $selmax = "SELECT codigoRegimen, descripcion FROM catalogo_regimen";
                                            $consultamax = mysqli_query($mysqli, $selmax);
                                            while ($row = mysqli_fetch_assoc($consultamax)) {
                                            ?>
                                                <option value="<?= $row['codigoRegimen'] ?>"><?= $row['codigoRegimen'] ?> - <?= $row['descripcion'] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-row">
                                    <div class="col-6">
                                        <label for="datepickermanual">Selecciona la fecha del ticket*</label>
                                        <input placeholder="Selecciona una fecha" type="date" id="datepickermanual" name="datepickermanual" class="form-control datepicker" required>
                                    </div>
                                    <div class="col-6">
                                        <label for="importemanual" data-error="Incorrecto" data-success="Correcto">Importe*</label>
                                        <input type="number" id="importemanual" name="importemanual" min="1" pattern="^[0-9]+" min="1" max="5000" class="form-control validate" required>
                                    </div>

                                    <div class="col-4">
                                        <br>
                                        <label for="fileinputmanual">Selecciona la foto de tu ticket*</label>
                                        <div class="file-field">
                                            <div class="btn btn-default btn-sm float-left">
                                                <span>Seleccionar archivo</span>
                                                <input type="file" name="fileinputmanual" id="fileinputmanual" accept="image/*" required>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" name="fileinputmanual" id="fileinputmanual" accept="image/*" type="text" placeholder="Cargar archivo" required>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-4">
                                        <br>
                                        <label for="fileinputcsf">Selecciona tu comprobante fiscal*</label>
                                        <div class="file-field">
                                            <div class="btn btn-default btn-sm float-left">
                                                <span>Seleccionar archivo</span>
                                                <input type="file" name="fileinputcsf" id="fileinputcsf" accept="application/pdf" required>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" name="fileinputcsf" id="fileinputcsf" accept="application/pdf" type="text" placeholder="Cargar archivo" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="selectpagomanual" name="selectpagomanual" required>
                                            <option value="">Elija su forma de pago*</option>
                                            <option value="01">(01) Efectivo</option>
                                            <option value="02">(02) Cheque nominativo</option>
                                            <option value="03">(03) Transferencia electrónica de fondos</option>
                                            <option value="04">(04) Tarjeta de crédito</option>
                                            <option value="05">(05) Monedero electrónico</option>
                                            <option value="06">(06) Dinero electrónico</option>
                                            <option value="08">(08) Vales de despensa</option>
                                            <option value="28">(28) Tarjeta de débito</option>
                                            <option value="29">(29) Tarjeta de servicio</option>
                                            <option value="99">(99) Otros</option>
                                        </select>
                                    </div>

                                    <div class="col-6">
                                        <br>
                                        <label for="foliomanual" data-error="Incorrecto" data-success="Correcto">Número boleto</label>
                                        <input type="number" id="foliomanual" name="foliomanual" min="1" pattern="^[0-9]+" class="form-control validate" required>
                                    </div>

                                    <div class="col-6">
                                        <br>
                                        <label for="correomanual" data-error="Incorrecto" data-success="Correcto">Correo*</label>
                                        <input type="email" id="correomanual" name="correomanual" class="form-control validate" required>
                                    </div>

                                </div>

                                <br>
                                <!-- Grid row -->

                                <button type="submit" class="btn white-text" style="background-color: #55B5A0;" onclick="comprueba_extension(this.form, this.form.archivoupload.value)">Enviar
                                </button>
                            </form>
                            <!-- Material column sizing form -->

                        </div>
                        <!-- Se muestra el formulario de automatico -->
                        <div id="automatico" style="display:none;">
                            <form id="formAutomatico" enctype="multipart/form-data">
                                <!-- Grid row -->
                                <div class="form-row">
                                    <!-- Grid column -->
                                    <div class="col-6">
                                        <!-- Material input -->
                                        <div class="md-form md-outline">
                                            <input type="text" id="rfcauto" name="rfcauto" class="form-control validate" minlength="12" maxlength="13" required>
                                            <input type="hidden" id="NoEstacionamientoAutomatico" name="NoEstacionamientoAutomatico">
                                            <label for="rfcauto" data-error="Incorrecto" data-success="Correcto">Escribe tu
                                                RFC*</label>
                                        </div>
                                    </div>
                                    <!-- Grid column -->

                                    <!-- Grid column -->
                                    <div class="col-6">
                                        <!-- Material input -->
                                        <div class="md-form md-outline">
                                            <input type="text" id="razonsocialauto" name="razonsocialauto" class="form-control validate" required>
                                            <label for="razonsocialauto" data-error="Incorrecto" data-success="Correcto">Razón
                                                social*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="md-form form-check">
                                    <p class="text-center grey-text mb-5 mx-auto w-responsive" style="font-size: 22px;">
                                        Datos
                                        Generales</p>
                                    <input type="checkbox" class="form-check-input" id="checkfiscauto" name="checkfiscauto">
                                    <label class="form-check-label" for="checkfiscauto">Pulse aqui para agregar dirección
                                        fiscal</label>
                                </div>
                                <!-- empieza formulario escondido -->
                                <style>
                                    #Check2 {
                                        display: none
                                    }
                                </style>
                                <!-- formulario oculto -->
                                <div id="Check2">
                                    <div class="form-row">
                                        <!-- Grid column -->
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="calleauto" name="calleauto" class="form-control validate">
                                                <label for="calleauto" data-error="Incorrecto" data-success="Correcto">Calle</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="noextauto" name="noextauto" class="form-control validate">
                                                <label for="noextauto" data-error="Ingrese solo letras y numeros" data-success="Correcto">No. Exterior</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="nointauto" name="nointauto" class="form-control validate">
                                                <label for="nointauto" data-error="Ingrese solo letras y numeros" data-success="Correcto">No. Interior</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="coloniaauto" name="coloniaauto" class="form-control validate">
                                                <label for="coloniaauto" data-error="Incorrecto" data-success="Correcto">Colonia</label>
                                            </div>
                                        </div>

                                        <!--<div class="col-3">

                                        <div class="md-form md-outline">
                                            <input type="text" id="pais" class="form-control validate">
                                            <label for="pais" data-error="Incorrecto" data-success="Correcto">Pais</label>
                                        </div>
                                    </div>-->
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="selectestadoauto" name="selectestadoauto">
                                                <option value="">Elija un estado</option>
                                                <option value="Aguascalientes">Aguascalientes</option>
                                                <option value="Baja California">Baja California</option>
                                                <option value="Baja California Sur">Baja California Sur</option>
                                                <option value="Campeche">Campeche</option>
                                                <option value="Coahuila de Zaragoza">Coahuila de Zaragoza</option>
                                                <option value="Colima">Colima</option>
                                                <option value="Chiapas">Chiapas</option>
                                                <option value="Chihuahua">Chihuahua</option>
                                                <option value="Ciudad de México">Ciudad de México</option>
                                                <option value="Durango">Durango</option>
                                                <option value="Guanajuato">Guanajuato</option>
                                                <option value="Guerrero">Guerrero</option>
                                                <option value="Hidalgo">Hidalgo</option>
                                                <option value="Jalisco">Jalisco</option>
                                                <option value="México">México</option>
                                                <option value="Michoacán de Ocampo">Michoacán de Ocampo</option>
                                                <option value="Morelos">Morelos</option>
                                                <option value="Nayarit">Nayarit</option>
                                                <option value="Nuevo León">Nuevo León</option>
                                                <option value="Oaxaca">Oaxaca</option>
                                                <option value="Puebla">Puebla</option>
                                                <option value="Querétaro">Querétaro</option>
                                                <option value="Quintana Roo">Quintana Roo</option>
                                                <option value="San Luis Potosí">San Luis Potosí</option>
                                                <option value="Sinaloa">Sinaloa</option>
                                                <option value="Sonora">Sonora</option>
                                                <option value="Tabasco">Tabasco</option>
                                                <option value="Tamaulipas">Tamaulipas</option>
                                                <option value="Tlaxcala">Tlaxcala</option>
                                                <option value="Veracruz de Ignacio de la Llave">Veracruz de Ignacio de la
                                                    Llave
                                                </option>
                                                <option value="Yucatán">Yucatán</option>
                                                <option value="Zacatecas">Zacatecas</option>
                                            </select>

                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="municipioauto" name="municipioauto" class="form-control validate">
                                                <label for="municipioauto" data-error="Ingrese solo letras y numeros" data-success="Correcto">Municipio/Delegación </label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="text" id="ciudadauto" name="ciudadauto" class="form-control validate">
                                                <label for="ciudadauto" data-error="Incorrecto" data-success="Correcto">Ciudad</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <!-- Material input -->
                                            <div class="md-form md-outline">
                                                <input type="number" id="codigopostalauto" name="codigopostalauto" min="1" pattern="^[0-9]+" class="form-control validate">
                                                <label for="codigopostalauto" data-error="Incorrecto" data-success="Correcto">Código
                                                    postal</label>
                                            </div>
                                        </div>

                                        <!-- termina row -->
                                    </div>

                                </div>

                                <p class="text-center grey-text mb-5 mx-auto w-responsive" style="font-size: 22px;">Uso de
                                    CFDI</p>
                                <div class="form-row">
                                    <div class="col-3">
                                        <!-- Material input -->
                                        <div class="md-form md-outline">
                                            <input type="number" id="codigopostalmanual" name="codigopostalmanual" class="form-control validate" min="1" pattern="^[0-9]+" required>
                                            <label for="codigopostal" data-error="Incorrecto" data-success="Correcto">Código
                                                postal*</label>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="selectconceptoauto" name="selectconceptoauto" required>
                                            <option value="">Elija un Uso CFDI*</option>
                                            <!-- <option value="G01">(G01) Adquisición de mercancias</option>
                                            <option value="G02">(G02) Devoluciones, descuentos o bonificaciones
                                            </option> -->
                                            <option value="G03">(G03) Gastos en general</option>
                                            <!-- <option value="P01">(P01) Por definir</option> -->
                                            <option value="S01">(S01) Sin efectos fiscales</option>
                                            <!-- <option value="I01">(I01) Construcciones</option>
                                            <option value="I02">(I02) Mobiliario y equipo de oficina por
                                                inversiones
                                            </option>
                                            <option value="I03">(I03) Equipo de transporte</option>
                                            <option value="I04">(I04) Equipo de computo y accesorios</option>
                                            <option value="I05">(I05) Dados, troqueles, moldes, matrices y
                                                herramental
                                            </option>
                                            <option value="I06">(I06) Comunicaciones telefónicas</option>
                                            <option value="I07">(I07) Comunicaciones satelitales</option>
                                            <option value="I08">(I08) Otra maquinaria y equipo</option>
                                            <option value="D01">(D01) Honorarios médicos, dentales y gastos
                                                hospitalarios.
                                            </option>
                                            <option value="D02">(D02) Gastos médicos por incapacidad o
                                                discapacidad
                                            </option>
                                            <option value="D03">(D03) Gastos funerales.</option>
                                            <option value="D04">(D04) Donativos.</option>
                                            <option value="D05">(D05) Intereses reales efectivamente pagados por
                                                créditos hipotecarios (casa habitación).
                                            </option>
                                            <option value="D06">(D06) Aportaciones voluntarias al SAR.</option>
                                            <option value="D07">(D07) Primas por seguros de gastos médicos.
                                            </option>
                                            <option value="D08">(D08) Gastos de transportación escolar
                                                obligatoria.
                                            </option>
                                            <option value="D09">(D09) Depósitos en cuentas para el ahorro, primas
                                                que
                                                tengan como base planes de pensiones.
                                            </option>
                                            <option value="D10">(D10) Pagos por servicios educativos
                                                (colegiaturas)
                                            </option> -->
                                            
                                        </select>
                                    </div>

                                    <div class="col-4">
                                        <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="regimenfiscal" name="regimenfiscal" required>
                                            <option value="">Elija un Regimen Fiscal*</option>
                                            <?php
                                            $selmax = "SELECT codigoRegimen, descripcion FROM catalogo_regimen";
                                            $consultamax = mysqli_query($mysqli, $selmax);
                                            while ($row = mysqli_fetch_assoc($consultamax)) {
                                            ?>
                                                <option value="<?= $row['codigoRegimen'] ?>"><?= $row['codigoRegimen'] ?> - <?= $row['descripcion'] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-row">
                                    <div class="col-4">
                                        <label for="datepickerauto">Fecha del ticket*</label>
                                        <input placeholder="Selecciona una fecha" type="text" id="datepickerauto" name="datepickerauto" class="form-control datepicker" required>
                                    </div>
                                    <div class="col-3">
                                        <label for="webidauto">Número boleto*</label>
                                        <input type="text" id="webidauto" name="webidauto" class="form-control" required>

                                    </div>
                                    <!--modal de ticket-->
                                    <div class="col-1">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-floating btn-default" data-toggle="modal" data-target="#basicExampleModalTicket">
                                            <i class="fas fa-question"></i>
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="basicExampleModalTicket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img data-src="img/boleto.webp" class="img-fluid lazyload" class="lazy">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                                            Cerrar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--modal de ticket-->
                                    <div class="col-4">
                                        <label for="importeauto">Importe*</label>
                                        <input type="number" id="importeauto" name="importeauto" class="form-control" min="1" max="5000" required>
                                    </div>

                                    <div class="col-6">
                                        <select class="mdb-select colorful-select dropdown-default md-form" searchable="Buscar aquí.." id="selectauto" name="selectpagoauto" required>
                                            <option value="">Elija su forma de pago*</option>
                                            <option value="01">(01) Efectivo</option>
                                            <option value="02">(02) Cheque nominativo</option>
                                            <option value="03">(03) Transferencia electrónica de fondos</option>
                                            <option value="04">(04) Tarjeta de crédito</option>
                                            <option value="05">(05) Monedero electrónico</option>
                                            <option value="06">(06) Dinero electrónico</option>
                                            <option value="08">(08) Vales de despensa</option>
                                            <option value="28">(28) Tarjeta de débito</option>
                                            <option value="29">(29) Tarjeta de servicio</option>
                                            <option value="99">(99) Otros</option>
                                        </select>
                                    </div>

                                    <div class="col-6">
                                        <br>
                                        <label for="correoauto" data-error="Incorrecto" data-success="Correcto">Correo*</label>
                                        <input type="email" id="correoauto" name="correoauto" class="form-control validate" required>
                                    </div>


                                </div>
                                <!-- Grid row -->
                                <br>
                                <button type="submit" class="btn btn-default">Enviar</button>
                                <!-- Material column sizing form -->
                            </form>
                        </div>
                        <br><br>
                        <div id="resultado"></div>

                    </div>


                </div>

            </section>
            <!-- Section: Team v.3 -->

            <hr class="my-5">

            <!--Seccion de pension -->

            <section id="products" class="text-center wow fadeIn" data-wow-delay="0.3s">

                <div class="card hoverable">

                    <div class="card-body">
                        <h2 class="font-weight-bold text-center h1 my-5">Facturar Pensión</h2>
                        <!--
                    <form action="Envio.php" method="post">
                        <div class="form-row">
                            <div class="col-3">
                                <div class="md-form md-outline">
                                    <input type="number" id="nopension" class="form-control validate" required>
                                    <label for="nopension" data-error="Incorrecto" data-success="Correcto">  de pensión</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="md-form md-outline">
                                    <input type="number" id="noestpension" class="form-control validate" required>
                                    <label for="noestpension" data-error="Incorrecto" data-success="Correcto">Numero de estacionamiento</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="md-form md-outline">
                                    <input type="text" id="rfcpen" class="form-control validate" required>
                                    <label for="rfcpen" data-error="Incorrecto" data-success="Correcto">Escribe tu
                                        RFC</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="md-form md-outline">
                                    <input type="number" id="importepen" class="form-control validate" required>
                                    <label for="importepen" data-error="Incorrecto, escriba un numero" data-success="Correcto">Importe</label>
                                </div>
                            </div>

                        </div>


                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </form>
                    -->
                        <div class="view jarallax" style="height: 100vh;">
                            <img class="jarallax-img" class="lazyload" data-src="img/background.webp" alt="Operadora central de estacionamientos" src="img/construccion.webp">
                            <div class="mask rgba-blue-slight">
                                <div class="container flex-center text-center">
                                    <div class="row mt-5">
                                        <div class="col-md-12 wow fadeIn mb-3">
                                            <h1 class="display-3 mb-2 wow fadeInDown" data-wow-delay="0.3s"><a class="white-text font-weight-bold">EN CONSTRUCCIÓN</a></h1>
                                            <h5 class="white-text text-uppercase mb-3 mt-1 font-weight-bold wow fadeIn" data-wow-delay="0.4s">PRÓXIMAMENTE.....</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>

            </section>

            <hr class="my-5">

            <!-- Seccion de contacto -->

            <section class="section pb-5" id="contact">

                <!--Section heading-->
                <h2 class="font-weight-bold text-center h1 my-5">Contáctanos</h2>
                <!--Section description-->
                <p class="section-description pb-4"></p>

                <div class="row">

                    <!--Grid column-->
                    <div class="col-lg-5 mb-4">

                        <!--Form with header-->
                        <div class="card hoverable">

                            <div class="card-body">
                                <!--Header-->
                                <div class="form-header aqua-gradient accent-1">
                                    <h3><i class="fas fa-phone-volume"></i> Atención a Clientes:</h3>
                                </div>


                                <!--Body-->
                                <div class="md-form">
                                    <a class="btn-floating accent-1" style="background-color: #55B5A0;"><i class="fas fa-phone"></i></a>
                                    <span>(55) 3640 - 3900</span>
                                </div>

                                <div class="md-form">
                                    <a class="btn-floating accent-1" style="background-color: #55B5A0;"><i class="fas fa-clipboard-list"></i></a>
                                    <span>Atención a Clientes</span>
                                </div>

                                <div class="md-form">
                                    <a class="btn-floating accent-1" style="background-color: #55B5A0;"><i class="fas fa-clipboard-list"></i></a>
                                    <span>Facturación</span>
                                </div>

                                <div class="md-form">
                                    <a class="btn-floating accent-1" style="background-color: #55B5A0;"><i class="fas fa-clipboard-list"></i></a>
                                    <span>Quejas</span>
                                </div>

                                <!--<div class="md-form">
                                <a class="btn-floating accent-1" style="background-color: #55B5A0;"><i
                                            class="fas fa-mobile-alt"></i></a>
                                <span><a href='https://wa.me/525554120526' target="_blank">Enviar mensaje a WhatsApp</a></span>
                                <strong><p>Nota: debes tener WhatsApp web abierto o la app instalada en tu dispositivo
                                    móvil.</p></strong>

                            </div>-->

                            </div>

                        </div>
                        <!--Form with header-->

                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-lg-7">

                        <!--Google map-->
                        <div id="map" style="height: 460px;"></div>

                        <br>
                        <!--Buttons-->
                        <div class="row text-center">
                            <div class="col-md-2">

                            </div>

                            <div class="col-md-4">
                                <a class="btn-floating accent-1" style="background-color: #55B5A0;"><i class="fas fa-phone"></i></a>
                                <p>(55) 3640-3900</p>
                            </div>

                            <div class="col-md-6">
                                <a class="btn-floating accent-1" style="background-color: #55B5A0;"><i class="fas fa-envelope"></i></a>
                                <p>facturacion@central-mx.com</p>
                            </div>
                        </div>

                    </div>
                    <!--Grid column-->

                </div>

            </section>

            <!-- modal para inicio de sesion -->
            <div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog cascading-modal" role="document">
                    <!--Content-->
                    <div class="modal-content">

                        <!--Modal cascading tabs-->
                        <div class="modal-c-tabs">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs md-tabs tabs-2 default-color-dark" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
                                        Iniciar Sesión</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
                                        Nuevo usuario</a>
                                </li>
                            </ul>

                            <!-- Tab panels -->
                            <div class="tab-content">
                                <!--Panel 7-->
                                <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                                    <form action="login/index" method="post">
                                        <!--Body-->
                                        <div class="modal-body mb-1">
                                            <div class="md-form form-sm mb-5">
                                                <i class="fas fa-envelope prefix"></i>
                                                <input type="email" name="mail_log" id="modalLRInput10" class="form-control form-control-sm validate">
                                                <label data-error="Incorrecto" data-success="Correcto" for="modalLRInput10" required>Correo</label>
                                            </div>

                                            <div class="md-form form-sm mb-4">
                                                <i class="fas fa-lock prefix"></i>
                                                <input type="password" name="pass_log" id="modalLRInput11" class="form-control form-control-sm validate" autocomplete="off" required>
                                                <label data-error="Incorrecto" data-success="Correcto" for="modalLRInput11">Contraseña</label>
                                            </div>
                                            <div class="text-center mt-2">
                                                <input type="submit" class="btn btn-default" value="Iniciar Sesion">
                                            </div>
                                            <br>

                                            <a href="recuperar"><i class="fas fa-id-card-alt"></i> Contraseña olvidada</a>
                                    </form>
                                </div>

                                <!--Footer-->
                                <div class="modal-footer">

                                    <button type="button" class="btn btn-outline-default waves-effect ml-auto" data-dismiss="modal">Cerrar
                                    </button>
                                </div>

                            </div>
                            <!--/.Panel 7-->

                            <!--Panel 8-->
                            <div class="tab-pane fade" id="panel8" role="tabpanel">

                                <!--Body-->
                                <div class="modal-body">
                                    <form action="login/registro" method="post">
                                        <!-- Grid row -->
                                        <div class="form-row">
                                            <!-- Grid column -->
                                            <div class="col-6">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="text" id="rfcreg" name="rfcreg" minlength="12" maxlength="13" class="form-control validate" required>
                                                    <label for="rfcreg" data-error="Incorrecto" data-success="Correcto">Escribe
                                                        tu RFC</label>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="text" id="razonsocialreg" name="razonsocialreg" class="form-control validate" required>
                                                    <label for="razonsocialreg" data-error="Incorrecto" data-success="Correcto">Razón
                                                        social</label>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="email" id="correoreg" name="correoreg" class="form-control validate" required>
                                                    <label for="correoreg" data-error="Incorrecto" data-success="Correcto">Correo</label>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <!-- Material input -->
                                                <div class="md-form md-outline">
                                                    <input type="password" id="passreg" name="passreg" class="form-control validate" autocomplete="off" required>
                                                    <label for="passreg" data-error="Incorrecto" data-success="Correcto">Contraseña</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-center form-sm mt-2">
                                            <button class="btn btn-default">Registrarse<i class="fas fa-sign-in ml-1"></i>
                                            </button>
                                        </div>
                                        <br>
                                        <p><strong>La contraseña debe contener lo siguiente:</strong></p>
                                        <li>Mínimo 8 caracteres</li>
                                        <li>Mínimo un número</li>
                                        <li>Mínimo una letra en mayúsculas</li>

                                    </form>

                                </div>
                                <!--Footer-->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-default waves-effect ml-auto" data-dismiss="modal">Cerrar
                                    </button>
                                </div>
                            </div>
                            <!--/.Panel 8-->
                        </div>

                    </div>
                </div>
                <!--/.Content-->
            </div>
        </div>


        </div>


        <!--Modal: modalPush-->
        <div class="modal fade" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-notify modal-warning" role="document">
                <!--Content-->
                <div class="modal-content text-center">
                    <!--Header-->
                    <div class="modal-header d-flex justify-content-center">
                        <p class="heading">Recordatorio</p>
                    </div>

                    <!--Body-->
                    <div class="modal-body">

                        <i class="fas fa-exclamation fa-4x animated rotateIn mb-4"></i>

                        <p>No olvides revisar el número de estacionamiento en nuestras sucursales</p>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer flex-center">
                        <a type="button" class="btn btn-warning waves-effect" data-dismiss="modal">Entendido</a>
                    </div>
                </div>
                <!--/.Content-->
            </div>
        </div>
        <!--Modal: modalPush-->

        <span style="display: none;"></span>

        <!-- Modal de preguntas frecuentes-->
        <div class="modal fade" id="ModalPreguntas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Preguntas Frecuentes</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>1. ¿Qué requisitos necesito para facturar?
                            - Ticket del estacionamiento
                            - Datos fiscales
                            - Correo electrónico
                        </p>
                        <p>2. ¿Cuánto tiempo pasa entre el uso del estacionamiento y la solicitud de la factura
                            electrónica?
                            24 hrs.</p>
                        <p>3. ¿Cuánto tiempo tengo para facturar después de usar el estacionamiento?
                            Preferentemente 72 hrs.</p>
                        <p>
                            4. ¿Por qué es recomendable registrarme en el Sistema de Facturación?
                            Porque me permite agilizar las solicitudes y búsqueda de facturas. Además te
                            permite contar con un histórico de todas las facturas solicitadas.
                        </p>
                        <p>
                            5. ¿Puedo facturar más de un ticket a la vez?
                            No</p>
                        <p>
                            6. ¿Puedo facturar tickets de diferentes estacionamientos en una sola factura?
                            No.</p>
                        <p>
                            7. ¿Puedo cancelar una factura?
                            Si y es necesario enviar una correo a facturacion@central-mx.com indicando el
                            motivo de la cancelación, el número de factura y la fecha de la misma.
                        </p>
                        <p>
                            8. ¿Cuál es el número de atención a clientes?
                            3640-3900
                        </p>
                        <p>
                            9. ¿Cuál es el correo electrónico de atención a clientes?
                            facturacion@central-mx.com
                        </p>
                        <p>
                            10. ¿Por qué no se visualiza mi imagen?
                            Es necesario que la imagen sea formato png, jpg, jpeg.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

    </main>
    <!-- Main Layout -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdb.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="preload" href="fonts/roboto/Roboto-Regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/roboto/Roboto-Bold.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/roboto/Roboto-Light.woff2" as="font" type="font/woff2" crossorigin>


    <!-- Footer -->
    <footer class="page-footer pt-4 mt-4 black text-center text-md-left">
        <a  class="class_a_href" style="pointer-events: none; cursor: default;"></a>
        <!-- Footer Links -->
        <div class="container">
            <div class="row">

                <!-- First column -->
                <div class="col-md-6">
                    <!-- Aqui va el certificado -->
                    <p></p>
                </div>
                <!-- First column -->
                <div style="padding-left: 250px; padding-top: 50px;">
                    <h6><a href="https://www.central-mx.com/aviso-de-privacidad">Aviso de privacidad</a></h6>
                </div>

            </div>
        </div>
        <!-- Footer Links -->

        <!-- Copyright -->
        <div class="footer-copyright py-3 text-center">
            <div class="container-fluid">
                <img data-data-src="img/logo/Logo.webp" class="lazyload" style="width: 60px; padding-right: 10px;" alt="Operadora Central de estacionamientos" />
                &copy; 2019 Copyright: <a href="https://www.central-mx.com/"> Operadora Central de Estacionamientos SAPI de
                    C.V</a>

            </div>
        </div>
        <!-- Copyright -->

    </footer>

    <script src="js/lazysizes.min.js" async=""></script>

    <!-- SCRIPTS -->
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
            let active = false;

            const lazyLoad = function() {
                if (active === false) {
                    active = true;

                    setTimeout(function() {
                        lazyImages.forEach(function(lazyImage) {
                            if ((lazyImage.getBoundingClientRect().top <= window.innerHeight && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== "none") {
                                lazyImage.src = lazyImage.dataset.src;
                                lazyImage.srcset = lazyImage.dataset.srcset;
                                lazyImage.classList.remove("lazy");

                                lazyImages = lazyImages.filter(function(image) {
                                    return image !== lazyImage;
                                });

                                if (lazyImages.length === 0) {
                                    document.removeEventListener("scroll", lazyLoad);
                                    window.removeEventListener("resize", lazyLoad);
                                    window.removeEventListener("orientationchange", lazyLoad);
                                }
                            }
                        });

                        active = false;
                    }, 200);
                }
            };

            document.addEventListener("scroll", lazyLoad);
            window.addEventListener("resize", lazyLoad);
            window.addEventListener("orientationchange", lazyLoad);
        });
    </script>
    <!-- JQuery -->
    <!--<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>-->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <script src="js/checkbox.js"></script>
    <script src="js/enviodb.js"></script>
    <script defer>
        function initMap() {
            var myLatLng = {
                lat: 19.359349,
                lng: -99.168890
            };
            var image = {

                url: 'img/logo/marker-central-2.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(30, 30),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(14, 30)
            };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 19,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                icon: image,
                map: map,
                title: 'Operadora Central de Estacionamientos'
            });
        }

        /*var map;
        function initMap() {
            var myLatLng = {lat: 19.4004616, lng: -99.1700712};
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 19.4004616, lng: -99.1700712},
                zoom: 19
            });


        }*/
    </script>


</body>

</html>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoN6cE0y9vrkzluAmHhRZVZXTACDqT2CI&callback=initMap" async defer></script>
<script>
    //select
    /*$(document).ready(function () {
        $('.mdb-select').materialSelect();
    });*/


    //modal al inicio de la aplicacion
    /*$( document ).ready(function() {
        $('#modalPush').modal('toggle')
    });*/
</script>