¡<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


<div class="container-fluid" style="padding-top: 140px;">
    <div id="env" class="col-md-4 col-md-offset-4">
        <center><img src="img/logo/logo/login-logo.png" style="width: 100px;" alt="Logo idea" class="img-responsive" /></center>
        <form action="login/indexgerente.php" method="post" >
            <div class="form-group">
                <label for="login">Correo:</label>
                <input type="text" id="login" name="mail_log" class="form-control" placeholder="Correo" required/>
                <label for="password">Contraseña:</label>
                <input type="password" id="password" name="pass_log" class="form-control" placeholder="Contraseña" autocomplete="off" required/>
            </div> <!-- form group -->
            <button type="submit" class="btn btn-lg btn-block">Entrar</button>
            <!--<a href="#!"><p class="text-center">Forgot password?</p></a>-->
        </form>
        <!--<a href="index"><i class="fas fa-arrow-circle-left"></i> Regresar a la pantalla principal</a>-->

    </div> <!-- #env -->
</div> <!-- container fluid -->