<?php
//error_reporting(0);

include 'conexion/conexion.php';
include 'composer/vendor/autoload.php';

use CfdiUtils\CadenaOrigen\DOMBuilder;
use CfdiUtils\XmlResolver\XmlResolver;
use Dompdf\Dompdf;
use Endroid\QrCode\QrCode;
use PHPMailer\PHPMailer\PHPMailer;

//var_dump($_POST);
//echo "rfc manual:";
$rfc = trim(strtoupper($_POST['rfcauto']));
$rfcmanual = str_replace("-", "", $rfc);

$razonsocial = trim(strtoupper($_POST['razonsocialauto']));
$calle = $_POST['calleauto'];
$noext = $_POST['noextauto'];
$noint = $_POST['nointauto'];
$colonia = $_POST['coloniaauto'];
$estado = $_POST['selectestadoauto'];
$municipio = $_POST['municipioauto'];
$ciudad = $_POST['ciudadauto'];
$cp = $_POST['codigopostalauto'];
$concepto = $_POST['selectconceptoauto'];
$fecha = $_POST['datepickerauto'];
$importe = $_POST['importeauto'];
$no_est = $_POST['NoEstacionamientoAutomatico'];
$email = $_POST['correoauto'];
$pagomanual = $_POST['selectpagoauto'];
$webid = $_POST['webidauto'];

$timestamp = date("Y-m-d H:i:s");
$newfechaq = date("Y-m-d H:i:s", strtotime($fecha));
$ftimbre = date("Y-m-d");
//echo "<br>";

$query = "SELECT fecha, monto, park, folio FROM `park_temps` WHERE park = '" . $no_est . "' AND folio = '" . $webid . "' ";
$consulta1 = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');
while ($fii = mysqli_fetch_array($consulta1)) {
    $fechaq = $fii['fecha'];
    $montoq = $fii['monto'];
    $folioq = $fii['folio'];
}

$newfechaq = date("d-m-Y", strtotime($fechaq));
//echo $newfechaq;


$con = "SELECT * FROM autof_tickets WHERE no_ticket = '" . $folioq . "'";
$consulta = mysqli_query($mysqli, $con) or die('Error al buscar en la base de datos.');
$row = mysqli_num_rows($consulta);


if ($row == 1) {
    ?>
    <div class="alert alert-danger" role="alert">
        Este ticket ya fue facturado anteriormente
        <p>Intente de nuevo</p>
    </div>
    <?php
    // header('location:../extend/alerta.php?msj=Este ticket ya fue ingresado&c=menu&p=menu&t=error');
} else {
    // echo "<br>";
    if ($fecha != $newfechaq || $importe != $montoq || $webid != $folioq) {
        ?>
        <div class="alert alert-danger" role="alert">
            No se encontro un ticket con la información ingresada
            <p>Intente de nuevo</p>
        </div>
        <?php
        //echo "no se encontro la informacion ingresada";
        //header('location:../extend/alerta.php?msj=No se encontro la informacion ingresada&c=menu&p=menu&t=error');
    } else {

        $newfecha = date("Y-m-d H:i:s", strtotime($fecha));


        $selmax = "SELECT MAX(factura) FROM autof_tickets";
        $consultamax = mysqli_query($mysqli, $selmax);
        $fmax = mysqli_fetch_assoc($consultamax);
        $idmax = $fmax['MAX(factura)'] + 1;

        $sql = "INSERT INTO autof_tickets(id, factura, no_ticket, total_ticket, fecha_emision, imagen, estatus, UsoCFDI, forma_pago, RFC, Razon_social, email, calle, no_ext, no_int, colonia, municipio, estado, cp, created_at, id_cliente, id_tipo, id_CentralUser, id_est) VALUES
    (NULL,'" . $idmax . "','" . $folioq . "','" . $importe . "','" . $newfecha . "',NULL , 'valido','" . $concepto . "','" . $pagomanual . "','" . $rfcmanual . "','" . $razonsocial . "','" . $email . "','" . $calle . "','" . $noext . "','" . $noint . "','" . $colonia . "','" . $municipio . "','" . $estado . "','" . $cp . "','" . $timestamp . "',1,2,1," . $no_est . ")";
        //echo "<br>";
        //echo "Paso insert";

        if ($mysqli->query($sql) === TRUE) {
            //header('location:../extend/alerta.php?msj=Envio exitoso&c=menu&p=menu&t=success');
            $timestamp = date("Y-m-d H:i:s");
            //echo "<br>";
            //echo "despliegue de consulta";
            $con_auto = "SELECT * FROM autof_tickets WHERE no_ticket = '" . $folioq . "'";
            $consulta_auto = mysqli_query($mysqli, $con_auto) or die('Error al buscar en la base de datos.');
            $row_auto = mysqli_num_rows($consulta_auto);
            //echo $con_auto;

            while ($f = mysqli_fetch_assoc($consulta_auto)) {
                $total_ticket = $f['total_ticket'];
                $UsoCFDI = $f['UsoCFDI'];
                $formapago = $f['forma_pago'];
                $id_estacionamiento = $f['id_est'];
            }

            //var_dump("Forma pago: ". $formapago);

            //echo "<br>";
            //echo "Termina despliegue";
            //echo $total_ticket;
            //echo "<br>";
            //echo "empieza segunda consulta";

            $con1 = "SELECT * FROM parks WHERE no_est = '" . $id_estacionamiento . "'";
            $consulta1 = mysqli_query($mysqli, $con1) or die('Error al buscar en la base de datos.');
            $row1 = mysqli_num_rows($consulta1);
            //echo $con1;

            while ($f1 = mysqli_fetch_assoc($consulta1)) {
                $folioPark = $f1['folio'];
                $seriePark = $f1['serie'];
                $id_org = $f1['id_org'];
                $razonsocialEmp = $f1['nombre'];
                $no_est = $f1['no_est'];
            }
            //echo "<br>";
            //echo "Termina desplieque de la segunda consulta";

            //echo "<br>";
            //echo "Comienza consulta orgs: ";
            //echo "<br>";
            $con2 = "SELECT * FROM orgs WHERE id = '" . $id_org . "'";
            $consulta2 = mysqli_query($mysqli, $con2) or die('Error al buscar en la base de datos.');
            $row2 = mysqli_num_rows($consulta2);

            while ($f2 = mysqli_fetch_assoc($consulta2)) {
                $RFC = $f2['RFC'];
                $Razon_social = $f2['Razon_social'];
                $Regimen_fiscal = $f2['Regimen_fiscal'];
            }
            //echo "Termina despliegue de orgs";
            $subtotal = number_format($total_ticket - ($total_ticket * .16), 2);
            $iva = number_format(($total_ticket - ($total_ticket * .16)) * .16, 2);

            //include "composer/vendor/autoload.php";
            $rutaCer = file_get_contents('process/lkey/00001000000508984781.cer');
            $rutaKey = file_get_contents('process/lkey/CSD_OPERADORA_CENTRAL_DE_ESTACIONAMIENTOS_SAPI_DE_CV_OCE9412073L3_20210913_181733.key');
            $pem = "-----BEGIN PRIVATE KEY-----<br>" . chunk_split(base64_encode($rutaKey), 64, "\n") . "<br>-----END PRIVATE KEY-----<br>-----BEGIN CERTIFICATE-----<br>\n" . chunk_split(base64_encode($rutaCer), 64, "\n") . "<br>-----END CERTIFICATE-----";

            $certificado = new \CfdiUtils\Certificado\Certificado('process/lkey/00001000000508984781.cer');

            $comprobanteAtributos = [
                'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/4',
                'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                'LugarExpedicion' => '03330', //codigo postal
                'MetodoPago' => 'PUE',
                'TipoDeComprobante' => 'I',
                'Total' => number_format($f['total_ticket'], 2),
                'Moneda' => 'MXN',
                'SubTotal' => $subtotal,
                'CondicionesDePago' => 'INMEDIATO',
                'FormaPago' => $formapago,
                'Exportacion' => '01',
                'Fecha' => $ftimbre . "T00:00:00",
                'Folio' => $folioPark + 1,
                'Serie' => $seriePark,
                'Version' => '4.0',
                'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd',
            ];
            $creator = new \CfdiUtils\CfdiCreator40($comprobanteAtributos, $certificado);
            $comprobante = $creator->comprobante();
            $comprobante->addEmisor([
                'RegimenFiscal' => $Regimen_fiscal,
                'Nombre' => $Razon_social,
                'Rfc' => $RFC,
            ]);
            $comprobante->addReceptor([
                'UsoCFDI' => $UsoCFDI,
                'Nombre' => $razonsocial,
                'Rfc' => $rfcmanual,
            ]);
            $comprobante->addConcepto([
                'ClaveProdServ' => '78111807',
                'Cantidad' => '1.00',
                'ClaveUnidad' => 'E48',
                'Unidad' => 'Unidad de servicio',
                'Descripcion' => 'Tarifas del Parqueadero',
                'ValorUnitario' => number_format($total_ticket - ($total_ticket * .16), 2),
                'Importe' => number_format($total_ticket - ($total_ticket * .16), 2),
            ])->addTraslado([
                'Importe' => number_format(($total_ticket - ($total_ticket * .16)) * .16, 2),
                'TasaOCuota' => '0.160000',
                'TipoFactor' => 'Tasa',
                'Impuesto' => '002',
                'Base' => number_format($total_ticket - ($total_ticket * .16), 2),
            ]);

            $creator->addSumasConceptos(NULL, 2);
            $key = file_get_contents('process/lkey/CSD_OPERADORA_CENTRAL_DE_ESTACIONAMIENTOS_SAPI_DE_CV_OCE9412073L3_20210913_181733.key.pem');
            $creator->addSello($key, 'CENTRAL3L394');
            $creator->saveXml('process/temporal/text.xml');
            $xml = $creator->asXml();
            //var_dump($xml);

            //////////////////////////////CONSUMO DE WEBSERVICEY//////////////////////////

            $xml2 = base64_encode($xml);
            $body = array(
                'xml' => $xml2,
            );

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.emite.app/v1/stamp",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($body),
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json; charset=UTF-8",
                    "Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI",
                    "Content-Type: application/json; charset=UTF-8"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return $err;
            } else {
                //var_dump($response);
                //echo "<br>";
                //echo "<br>";
                //echo "Resultado: ";
                //$json = json_decode($response);
                //print_r($json);
                $val = json_decode($response);
                if ($val->data->status == "previously_stamped" || $val->data->status == "stamped") {
                    $xml3 = $val->data->document_info->xml;
                    $uuid = $val->data->stamp_info->uuid;
                    $xml3 = base64_decode($xml3);
                    file_put_contents("process/temporal/" . $uuid . ".xml", $xml3);
                    $ochocarct = substr($uuid, -8);

                    $xmlContent = file_get_contents("process/temporal/" . $uuid . ".xml");
                    $resolver = new XmlResolver();
                    $location = $resolver->resolveCadenaOrigenLocation('3.3');
                    $builder = new DOMBuilder();
                    $cadenaorigen = $builder->build($xmlContent, $location);

                    $xmlContents = $xml3;
                    $cfdi = \CfdiUtils\Cfdi::newFromString($xmlContents);
                    $cfdi->getVersion(); // (string) 3.3
                    $cfdi->getDocument(); // clon del objeto DOMDocument
                    $cfdi->getSource(); // (string) <cfdi:Comprobante...
                    $complemento = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Comprobante
                    $tfd = $complemento->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
                    $selloSAT = $tfd['SelloSAT'];
                    $selloCFD = $tfd['SelloCFD'];
                    $fechatimbrado = $tfd['FechaTimbrado'];
                    $csdEmisor = $complemento['NoCertificado'];
                    $certSat = $tfd['NoCertificadoSAT'];

                    $qrCadena = 'https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=' . $uuid . '&re=' . $RFC . '&rr=' . $rfcmanual . '&tt=' . $total_ticket . '0000';
                    $qrCode = new QrCode($qrCadena);
                    header('Content-Type: ' . $qrCode->getContentType());
                    $qrCode->writeString();
                    $ubiQr = "process/pdf-template/" . $uuid . "qrcode.png";
                    $ubicacion = $qrCode->writeFile($ubiQr);

                }
            }

            ob_start();
            ?>
            <!DOCTYPE html>
            <html lang="es">
            <head>
                <meta charset="utf-8">
                <link rel="stylesheet" href="process/pdf-template/style.css" media="all"/>
            </head>
            <body>
            <header class="clearfix">
                <div id="logo">
                    <img src="process/pdf-template/img/logo.png">
                </div>
                <h1>DATOS FACTURA</h1>
                <h4 align="center">DATOS EMPRESA</h4>
                <table style="text-align:center;">
                    <thead>
                    <tr>
                        <th>Razón social</th>
                        <th>RFC</th>
                        <th>Dirección</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="service"><?= $razonsocialEmp ?></td>
                        <td class="desc"><?= $RFC ?></td>
                        <td class="unit">AV INSURGENTES SUR 1863 301-B GUADALUPE INN. ALVARO OBREGON CIUDAD DE MÉXICO CP
                            01020
                        </td>
                    </tr>
                    </tbody>
                </table>
            </header>
            <h4 align="center">DATOS CLIENTE</h4>
            <table style="text-align:center;">
                <thead>
                <tr>
                    <th>Cliente</th>
                    <th>RFC</th>
                    <!--<th>Dirección</th>
                    <th>Codigo Postal</th>-->
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="service"><?= $razonsocial ?></td>
                    <td class="desc"><?= $rfcmanual ?></td>
                    <!--<td class="unit"><?= $calle ?></td>
                    <td class="qty"><?= $cp ?>< /td>-->
                </tr>
                </tbody>
            </table>
            <h4 align="center">FACTURA TIPO (I)</h4>
            <table style="text-align:center;">
                <thead>
                <tr>
                    <th>Serie-folio</th>
                    <th>Folio fiscal</th>
                    <th>No.Serie del CSD Emisor</th>
                </tr>
                </thead>
                </tr>
                <tr>
                    <td><?= $seriePark . $folioPark + 1 ?></td>
                    <td><?php echo $uuid ?></td>
                    <td><?php echo $csdEmisor ?></td>
                </tr>
                <tr>
                    <th>Fecha y Hora de emisión</th>
                    <th>Fecha y Hora de certificación</th>
                    <th>No.Serie del CSD del SAT</th>
                </tr>
                <tr>
                    <td><?= $timestamp ?></td>
                    <td><?= $fechatimbrado ?></td>
                    <td><?= $certSat ?></td>

                </tr>
                </tbody>
            </table>
            <table style="text-align:center;">
                <thead>
                <tr>
                    <th>Uso CFDI</th>
                    <th>Método de Pago</th>
                    <th>Forma de Pago</th>
                    <th>Régimen fiscal</th>
                </tr>
                </thead>
                </tr>
                <tr>
                    <td><?= $concepto ?></td>
                    <td>PUE</td>
                    <td><?= $formapago ?></td>
                    <td>General de Ley Personas Morales(601)</td>
                </tr>
                </tbody>
            </table>
            <h4 align="center">CONCEPTOS</h4>
            <main>
                <table style="text-align:center;">
                    <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Unidad</th>
                        <th>Precio Unitario</th>
                        <th>Descuento</th>
                        <th>Importe</th>
                        <th>IVA</th>
                        <th>IEPS</th>
                        <th>IMP.EST.</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="service">1</td>
                        <td class="desc">E48/Unidad de servicio</td>
                        <td class="unit"><?= number_format($total_ticket - ($total_ticket * .16), 2) ?></td>
                        <td class="qty"></td>
                        <td class="total"><?= $importe ?></td>
                        <td><?= number_format($total_ticket - ($total_ticket * .16), 2) ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>

                <table style="text-align:center;">
                    <thead>
                    <tr>
                        <th>Total</th>
                        <th>subTotal</th>
                        <th>I.V.A. 16%</th>
                        <th>Total IVA</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="service"><?= $total_ticket ?></td>
                        <td class="desc"><?= $subtotal ?></td>
                        <td class="unit"><?= $iva ?></td>
                        <td class="qty"><?= $iva ?></td>
                        <td class="total"><?= $total_ticket ?></td>
                    </tr>
                    </tbody>
                </table>

                <div id="notices">
                    <div class="notice">"LA ALTERACIÓN, FALSIFICACIÓN O COMERCIALIZACIÓN ILEGAL DE ESTE DOCUMENTO ESTA
                        PENADO POR LA LEY".
                    </div>
                    <table style="text-align:center;">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Cadena original</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><img src="<?= $ubiQr ?>" style="width: 140px;"></td>
                            <td class="service"><?= $cadenaorigen ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <p>Sello SAT</p>
                <p><?= $registro_salto = wordwrap($selloSAT, 120, "\n", true); ?></p>
                <p>Sello CFD</p>
                <p><?= $registro_salto1 = wordwrap($selloCFD, 120, "\n", true); ?></p>
                <p>Estacionamiento: <?= $no_est ?></p>
                <p>"Este documento es una representación impresa de un CFDI"</p>
                </div>
            </main>

            </body>
            </html>
            <?php
            $dompdf = new Dompdf();
            $dompdf->loadHtml(ob_get_clean());
            $dompdf->setPaper('DEFAULT_PDF_PAPER_SIZE', 'portrait');
            ini_set("memory_limit", "256M");
            $dompdf->render();
            //$dompdf->stream('temporal/prueba.pdf');
            $pdf = $dompdf->output();
            file_put_contents("process/temporal/" . $uuid . ".pdf", $pdf);
            /////////TERMINA PDF///////////////////////////////

            $folioPark1 = $folioPark + 1;
            $im = file_get_contents($pdf);
            $pdf64 = base64_encode($im);

            /*$selmax1 = "SELECT MAX(id) FROM autof_tickets";
            $consultamax1 = mysqli_query($mysqli, $selmax1);
            $fmax1 = mysqli_fetch_assoc($consultamax1);
            $idmax1 = $fmax1['MAX(id)'];*/

            $sql_auto = "INSERT INTO auto_facts (serie, tipo_doc,folio, fecha_timbrado, uuid ,subtotal_factura,iva_factura, total_factura, XML, PDF,  estatus, created_at,id_ticketAF) VALUES
    ('" . $seriePark . "','I','" . $folioPark1 . "', '" . $timestamp . "', '" . $uuid . "' ,'" . $subtotal . "','" . $iva . "','" . $total_ticket . "','" . $xml3 . "','" . $pdf64 . "', 'timbrada', '" . $timestamp . "' , '" . $idmax . "')";
            if ($mysqli->query($sql_auto) === TRUE) {
                //UPDATE `parks` SET `folio`= 1 WHERE no_est = 8
                //mysqli_query("UPDATE parks SET folio = 5 WHERE  no_est = '".$id_estacionamiento."' ");
                $sql_parks = "UPDATE parks SET folio = '" . $folioPark1 . "' WHERE  no_est = '" . $id_estacionamiento . "' ";

                if (mysqli_query($mysqli, $sql_parks)) {
                    //inicia envio de correo

                    $mail = new PHPMailer;
                    $mail->isSMTP();
                    $mail->SMTPDebug = 0;
                    $mail->Debugoutput = 'html';
                    /*$mail->Host = 'smtp.gmail.com';
                    $mail->Port = 587;
                    $mail->SMTPSecure = 'tls';
                    $mail->SMTPAuth = true;
                    $mail->Username = "ocefacturacion@gmail.com";
                    $mail->Password = "Integrador1";
                    $mail->setFrom('ocefacturacion@gmail.com', 'Central operadora de estacionamientos');*/
                    $mail->Host = 'smtp.office365.com';
                    $mail->Port = 587;
                    $mail->SMTPSecure = 'tls';
                    $mail->SMTPAuth = true;
                    $mail->Username = "facturacion-oce@central-mx.com";
                    $mail->Password = "1t3gr4d0r2020*";
                    $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
                    $mail->Subject = "Factura " . $seriePark . $folioPark;
                    $mail->Body = "<html><h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
 
<p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: $uuid, Serie: $seriePark y Folio: $folioPark. Así como su representación impresa.</p>

<p align='center;'><strong>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</strong></p>

<p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico $email a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
                    $mail->AddAddress($email);
                    $archivo = 'process/temporal/' . $uuid . '.xml';
                    $pdf = 'process/temporal/' . $uuid . '.pdf';
                    $mail->AddAttachment($archivo);
                    $mail->AddAttachment($pdf);
                    $mail->IsHTML(true);
                    $mail->Send();

                    $archivo = "Facturas/" . $uuid . ".xml";
                    $pdf1 = "Facturas/" . $uuid . '.pdf';
                    $mail->AddAttachment($archivo);
                    $mail->AddAttachment($pdf1);
                    $mail->IsHTML(true);
                    $mail->Send();
                    //echo "paso";
                    //echo "se envio factura";
                    unlink($archivo);
                    unlink($pdf);
                    unlink($ubiQr);
                    echo "¡Exito!";
                } else {
                    ?>
                    <div class="alert alert-danger" role="alert">
                        Ocurrio un error al enviar la información
                        <p>Intente de nuevo</p>
                    </div>
                    <?php
                }

                //  header('location:../extend/alerta.php?msj=Su factura fue enviada exitosamente a su correo electronico&c=menu&p=menu&t=success');
            } else {
                //header('location:../extend/alerta.php?msj=Ocurrio un error, intente de nuevo&c=menu&p=menu&t=error');
                ?>
                <div class="alert alert-danger" role="alert">
                    Ocurrio un error al enviar la información
                    <p>Intente de nuevo</p>
                </div>
                <?php
                //echo "fallo";
                //echo $mysqli->error;
            }
            ////////////TERmina envio de mail////////////////////////////
            //echo '<p>Se ha enviado correctamente el email a '.$mail.'!</p>';

        }//termina else de insert
        else {
            //echo "Intente de nuevo";
            ?>
            <div class="alert alert-danger" role="alert">
                Ocurrio un error al enviar la información
                <p>Intente de nuevo</p>
            </div>
            <?php
            echo "segundo error";
            //header('location:../extend/alerta.php?msj=Intente de nuevo&c=menu&p=menu&t=error');
        }


    }//termina else de informacion ingresada

}//termina else de row