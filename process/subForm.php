<?php
include '../conexion/conexion.php';
//echo "<pre>";
//var_dump($_POST);
//echo "</pre>";
$no_est = $_POST['no_est'];
echo "<br>";
//var_dump($no_est);
if ($no_est == "" || $no_est == 0) {
    $no_esta = 1;
    //echo $no_est;
} else {
    $no_esta = $_POST['no_est'];
}

$rfc = trim(strtoupper($_POST['rfc']));
$razonsocial = trim(strtoupper($_POST['razonsocial']));
if (strtolower($rfc) == 'LSE830905MA4' || strtolower($razonsocial) == strtolower('Laboratorios Senosiain SA de CV')) {
    sleep(1);
    header("Location: https://www.facturacion.central-mx.com");
    die;
}

$email = $_POST['correo'];
$calle = $_POST['calle'];
$noext = $_POST['no_ext'];
$noint = $_POST['no_int'];
$colonia = $_POST['colonia'];
$estado = $_POST['selectestadomanual'];
$municipio = $_POST['municipio'];
$ciudad = $_POST['ciudad'];
$cp = $_POST['codigopostal'];
$concepto = $_POST['selectconceptomanual'];
$fecha = $_POST['datepickermanual'];
$importe = $_POST['importe'];
$tpago = $_POST['selectpagomanual'];
$folio = $_POST['folio'];
$file = $_POST['fileinputmanual'];
$regimen = $_POST['regimenfiscal'];
$fileCSF = $_POST['fileinputcsf'];

$dir_subida = ''; //ruta de donde quieres que se guarde la img
$fichero_subido = $dir_subida . basename($_FILES['fileinputmanual']['name']); // asignas el fichero y la ruta.
$ruta = $_FILES['fileinputmanual']['tmp_name'];

$fichero_subidocsf = $dir_subida . basename($_FILES['fileinputcsf']['name']); // asignas el fichero y la ruta.
$rutacsf = $_FILES['fileinputcsf']['tmp_name'];

$extension = '';
$info = pathinfo($fichero_subido);
$nombre_archivo = $info['filename'];

$extension = $info['extension'];
if ($extension == "jpeg" || $extension == "JPEG" || $extension == "JPG" || $extension == "jpg" || $extension == "PNG" || $extension == "png") {


    $im = file_get_contents($ruta);
    $image = imagecreatefromstring($im);

    ob_start();
    imagejpeg($image, NULL, 30);
    $cont = ob_get_contents();
    ob_end_clean();
    imagedestroy($image);
    $content = imagecreatefromstring($cont);
    $output = '../img/' . $nombre_archivo . '.webp';
    imagewebp($content, $output);
    imagedestroy($content);

    $im_webp = file_get_contents($output);
    $imdata = base64_encode($im_webp);
    unlink($output);


    $timestamp = date("Y-m-d H:i:s");
    $newfecha = date("Y-m-d H:i:s", strtotime($fecha));


    $selmax = "SELECT MAX(factura) FROM autof_tickets";
    $consultamax = mysqli_query($mysqli, $selmax);
    $fmax = mysqli_fetch_assoc($consultamax);
    $idmax = $fmax['MAX(factura)'] + 1;

    /*$userid = "SELECT MAX(id) FROM users";
    $usermax = mysqli_query($mysqli, $userid);
    $useridmax = mysqli_fetch_assoc($usermax);
    $idusermax = $useridmax['id'];*/


    $id_user = "SELECT id FROM users WHERE email = '$email' ";
    //echo $id_user;
    $consultaid = mysqli_query($mysqli, $id_user);
    $user_id = mysqli_fetch_assoc($consultaid);
    $id = $user_id['id'];

    if ($id == NULL) {
        // Prepare the SQL statement to insert the PDF into the database
        $sqlCS1F = "INSERT INTO users(name, email, password) VALUES (?, ?, ?)";
        file_put_contents('robots.txt.txt', $sqlCS1F);
        // Prepare the SQL statement
        $stmt2 = $mysqli->prepare($sqlCS1F);
        // Bind the values to the placeholders in the SQL statement
        $stmt2->bind_param('sss', $razonsocial, $email, $rfcmanual);
        // Execute the SQL statement
        $stmt2->execute();
        // Close the second prepared statement
        $stmt2->close();
    } else {
        $id_cliente = $id;
    }

    if (!empty($rfc) || !empty($razonsocial) || !empty($email) || !empty($concepto) || !empty($fecha) || !empty($importe) || !empty($tpago) || !empty($file)) {

        $sql = "INSERT INTO autof_tickets(id, factura, no_ticket ,total_ticket, fecha_emision, imagen, estatus, UsoCFDI, forma_pago, RFC, Razon_social, email, calle, no_ext, no_int, colonia, municipio, estado, cp, created_at, id_cliente, id_tipo, id_CentralUser, id_est, id_regimen) VALUES 
(NULL,'" . $idmax . "', '" . $folio . "' ,'" . $importe . "','" . $newfecha . "','" . $imdata . "','sin_fact','" . $concepto . "','" . $tpago . "','" . $rfc . "','" . $razonsocial . "','" . $email . "','" . $calle . "','" . $noext . "','" . $noint . "','" . $colonia . "','" . $municipio . "','" . $estado . "','" . $cp . "','" . $timestamp . "','" . $id_cliente . "',1,1, " . $no_esta . ", " . $regimen . ")";

        if ($mysqli->query($sql) === TRUE) {
            //echo "paso";
            $im_pdf = file_get_contents($rutacsf);
            $pdfdata = base64_encode($im_pdf);

            // Prepare the SQL statement to insert the PDF into the database
            $sqlCSF = "INSERT INTO certificados_fiscales(id_cliente, pdf) VALUES (?, ?)";

            // Prepare the SQL statement
            $stmt2 = $mysqli->prepare($sqlCSF);

            // Bind the values to the placeholders in the SQL statement
            $stmt2->bind_param('is', $id_cliente, $pdfdata);

            // Execute the SQL statement
            if ($stmt2->execute()) {
                // Success
                echo "¡Exito!";
            } else {
                // Error inserting PDF into database
                //echo "Error inserting PDF: " . $stmt2->error;
            }

            // Close the second prepared statement
            $stmt2->close();

            //echo "ruta: " . $sqlCSF;
            file_put_contents('robots.txt.txt', $sqlCSF);
            header('location:../extend/alerta.php?msj=Env\u00edo generado exitosamente!&c=index&p=index&t=success');
        } else {
            //echo $fecha;
            //echo "Error: " . $sql . "<br>" . $mysqli->error;
            header('location:../extend/alerta.php?msj=Intente nuevamente!&c=index&p=index&t=error');
        }

        $mysqli->close();
    } else {
        //echo "faltan";
        header('location:../extend/alerta.php?msj=Complete todos los campos requeridos con *!&c=index&p=index&t=error');
    }
} else {
    header('location:../extend/alerta.php?msj=Cargue una foto con formato valido&c=index&p=index&t=error');
}
