<?php
include '../conexion/conexion.php';
error_reporting(0);
include '../composer/vendor/autoload.php';
Sentry\init(['dsn' => 'https://da2ed1a4b3c04fd789c70efa6dcc13d9@o514087.ingest.sentry.io/5616842' ]);

use CfdiUtils\CadenaOrigen\DOMBuilder;
use CfdiUtils\XmlResolver\XmlResolver;
use Dompdf\Dompdf;
use Endroid\QrCode\QrCode;
use PHPMailer\PHPMailer\PHPMailer;


//var_dump($_POST);
//echo "<br>";
$concepto = $_POST['conceptoauto'];
$pago = $_POST['pagoauto'];
$importe = $_POST['importeauto'];
$fecha = $_POST['dateauto'];
$webid = $_POST['webid'];
$razonsocial = $_POST['razonsocial'];
$calle = $_POST['calle'];
$no_ext = $_POST['no_ext'];
$no_int = $_POST['no_int'];
$colonia = $_POST['colonia'];
$municipio = $_POST['municipio'];
$estado = $_POST['estado'];
$cp = $_POST['cp'];
$rfc = strtoupper($_POST['rfc']);
$no_est = $_POST['no_est'];
$email = $_POST['correo'];

$timestamp = date("Y-m-d H:i:s");
/*echo "consulta";
echo "<br>";
echo $fecha;*/
//convierte a timestamp cualquier fecha
$newfechaq = date("Y-m-d H:i:s", strtotime($fecha));
$ftimbre = date("Y-m-d");
//echo "<br>";

$query = "SELECT fecha, monto, park, folio FROM `park_temps` WHERE park = '" . $no_est . "' AND folio = '" . $webid . "' ";
$consulta1 = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');
while ($fii = mysqli_fetch_array($consulta1)) {
    $fechaq = $fii['fecha'];
    $montoq = $fii['monto'];
    $folioq = $fii['folio'];
}

$newfechaq = date("d-m-Y", strtotime($fechaq));
//echo $newfechaq;


$con = "SELECT * FROM autof_tickets WHERE no_ticket = '" . $folioq . "'";
$consulta = mysqli_query($mysqli, $con) or die('Error al buscar en la base de datos.');
$row = mysqli_num_rows($consulta);

if ($row == 1) {
    //echo "<br>";
    //echo "este ticket ya fue ingresado";
    header('location:../extend/alerta?msj=Este ticket ya fue ingresado&c=menu&p=menu&t=error');
} else {
    //echo "<br>";
    if ($fecha != $newfechaq || $importe != $montoq || $webid != $folioq) {
        //echo "no se encontro la informacion ingresada";
        header('location:../extend/alerta?msj=No se encontro la información ingresada&c=menu&p=menu&t=error');

    } else {

        $newfecha = date("Y-m-d H:i:s", strtotime($fecha));


        $selmax = "SELECT MAX(factura) FROM autof_tickets";
        $consultamax = mysqli_query($mysqli, $selmax);
        $fmax = mysqli_fetch_assoc($consultamax);
        $idmax = $fmax['MAX(factura)'] + 1;

        $id_user = "SELECT id FROM users WHERE email = '$email' ";
//echo $id_user;
        $consultaid = mysqli_query($mysqli, $id_user);
        $user_id = mysqli_fetch_assoc($consultaid);
        $id = $user_id['id'];

        $sql = "INSERT INTO autof_tickets(id, factura, no_ticket, total_ticket, fecha_emision, imagen, estatus, UsoCFDI, forma_pago, RFC, Razon_social, email, calle, no_ext, no_int, colonia, municipio, estado, cp, created_at, id_cliente, id_tipo, id_CentralUser, id_est) VALUES
    (NULL,'" . $idmax . "','" . $folioq . "','" . $importe . "','" . $newfecha . "',NULL , 'valido','" . $concepto . "','" . $pago . "','" . $rfc . "','" . $razonsocial . "','" . $email . "','" . $calle . "','" . $no_ext . "','" . $no_int . "','" . $colonia . "','" . $municipio . "','" . $estado . "','" . $cp . "','" . $timestamp . "','" . $id . "',2,1," . $no_est . ")";
        //echo "<br>";
        //echo "Paso insert";

        if ($mysqli->query($sql) === TRUE) {
            //header('location:../extend/alerta.php?msj=Envio exitoso&c=menu&p=menu&t=success');
            $timestamp = date("Y-m-d H:i:s");
            //echo "<br>";
            //echo "despliegue de consulta";
            $con_auto = "SELECT * FROM autof_tickets WHERE no_ticket = '" . $folioq . "'";
            $consulta_auto = mysqli_query($mysqli, $con_auto) or die('Error al buscar en la base de datos.');
            $row_auto = mysqli_num_rows($consulta_auto);
            //echo $con_auto;

            while ($f = mysqli_fetch_assoc($consulta_auto)) {
                $total_ticket = $f['total_ticket'];
                $UsoCFDI = $f['UsoCFDI'];
                $formapago = $f['forma_pago'];
                $id_estacionamiento = $f['id_est'];
            }
            /*echo "<br>";
            echo "Termina despliegue";
            echo $total_ticket;
            echo "<br>";
            echo "empieza segunda consulta";*/

            $con1 = "SELECT * FROM parks WHERE no_est = '" . $id_estacionamiento . "'";
            $consulta1 = mysqli_query($mysqli, $con1) or die('Error al buscar en la base de datos.');
            $row1 = mysqli_num_rows($consulta1);
            //echo $con1;

            while ($f1 = mysqli_fetch_assoc($consulta1)) {
                $folioPark = $f1['folio'];
                $seriePark = $f1['serie'];
                $id_org = $f1['id_org'];
                $razonsocialEmp = $f1['nombre'];
            }
            /*echo "<br>";
            echo "Termina desplieque de la segunda consulta";

            echo "<br>";
            echo "Comienza consulta orgs: ";
            echo "<br>";*/
            $con2 = "SELECT * FROM orgs WHERE id = '" . $id_org . "'";
            $consulta2 = mysqli_query($mysqli, $con2) or die('Error al buscar en la base de datos.');
            $row2 = mysqli_num_rows($consulta2);

            while ($f2 = mysqli_fetch_assoc($consulta2)) {
                $RFC = $f2['RFC'];
                $Razon_social = $f2['Razon_social'];
                $Regimen_fiscal = $f2['Regimen_fiscal'];
            }
            //echo "Termina despliegue de orgs";
            $subtotal = str_replace(",", "", number_format($total_ticket['total_ticket'] / 1.16, 2));
            //$iva = str_replace(",", "", number_format(($total_ticket['total_ticket'] / 1.16) * .16, 2));

            //include "../composer/vendor/autoload.php";
            $rutaCer = file_get_contents('lkey/00001000000503977924.cer');
            $rutaKey = file_get_contents('lkey/CSD_OPERADORA_CENTRAL_DE_ESTACIONAMIENTOS_SAPI_DE_CV_OCE9412073L3_20160520_091056.key');
            //$pem = "-----BEGIN PRIVATE KEY-----<br>" . chunk_split(base64_encode($rutaKey), 64, "\n") . "<br>-----END PRIVATE KEY-----<br>-----BEGIN CERTIFICATE-----<br>\n" . chunk_split(base64_encode($rutaCer), 64, "\n") . "<br>-----END CERTIFICATE-----";

            $certificado = new \CfdiUtils\Certificado\Certificado('lkey/00001000000503977924.cer');

            $comprobanteAtributos = [
                'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/3',
                'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                'LugarExpedicion' => '03330', //codigo postal
                'MetodoPago' => 'PUE',
                'TipoDeComprobante' => 'I',
                'Total' => number_format($f['total_ticket'], 2),
                'Moneda' => 'MXN',
                'SubTotal' => $subtotal,
                'FormaPago' => $formapago,
                'Fecha' => $ftimbre . "T00:00:00",
                'Folio' => $folioPark + 1,
                'Serie' => $seriePark,
                'Version' => '3.3',
                'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
            ];
            $creator = new \CfdiUtils\CfdiCreator33($comprobanteAtributos, $certificado);
            $comprobante = $creator->comprobante();
            $comprobante->addEmisor([
                'RegimenFiscal' => $Regimen_fiscal,
                'Nombre' => $Razon_social,
                'Rfc' => $RFC,
            ]);
            $comprobante->addReceptor([
                'UsoCFDI' => $UsoCFDI,
                'Nombre' => $razonsocial,
                'Rfc' => $rfc,
            ]);
            $comprobante->addConcepto([
                'ClaveProdServ' => '78111807',
                'Cantidad' => '1.00',
                'ClaveUnidad' => 'E48',
                'Unidad' => 'Unidad de servicio',
                'Descripcion' => 'Tarifas del Parqueadero',
                'ValorUnitario' => str_replace(",", "", number_format($total_ticket['total_ticket'] / 1.16, 2)),
                'Importe' => str_replace(",", "", number_format($total_ticket['total_ticket'] / 1.16, 2)),
            ])->addTraslado([
                'Importe' => str_replace(",", "", number_format(($total_ticket['total_ticket'] / 1.16) * .16, 2)),
                'TasaOCuota' => '0.160000',
                'TipoFactor' => 'Tasa',
                'Impuesto' => '002',
                'Base' => str_replace(",", "", number_format($total_ticket['total_ticket'] / 1.16, 2)),
            ]);

            $creator->addSumasConceptos(NULL, 2);
            $key = file_get_contents('lkey/CSD_UNIDAD_OCE9412073L3_20200514_111724.key.pem');
            $creator->addSello($key, 'OCE94120');
            //$creator->saveXml('temporal/text.xml');
            $xml = $creator->asXml();

            //////////////////////////////CONSUMO DE WEBSERVICEY//////////////////////////

            $xml2 = base64_encode($xml);
            $body = array(
                'xml' => $xml2,
            );

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.emite.app/v1/stamp",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($body),
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json; charset=UTF-8",
                    "Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI",
                    "Content-Type: application/json; charset=UTF-8"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return $err;
            } else {
                /*var_dump($response);
                echo "<br>";
                echo "<br>";
                echo "Resultado: ";
                //$json = json_decode($response);
                //print_r($json);*/
                $val = json_decode($response);
                if ($val->data->status == "previously_stamped" || $val->data->status == "stamped") {
                    $xml3 = $val->data->document_info->xml;
                    $uuid = $val->data->stamp_info->uuid;
                    $xml3 = base64_decode($xml3);
                    file_put_contents("temporal/" . $uuid . ".xml", $xml3);
                    $ochocarct = substr($uuid, -8);

                    $xmlContent = file_get_contents("temporal/" . $uuid . ".xml");
                    $resolver = new XmlResolver();
                    $location = $resolver->resolveCadenaOrigenLocation('3.3');
                    $builder = new DOMBuilder();
                    $cadenaorigen = $builder->build($xmlContent, $location);

                    $xmlContents = $xml3;
                    $cfdi = \CfdiUtils\Cfdi::newFromString($xmlContents);
                    $cfdi->getVersion(); // (string) 3.3
                    $cfdi->getDocument(); // clon del objeto DOMDocument
                    $cfdi->getSource(); // (string) <cfdi:Comprobante...
                    $complemento = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Comprobante
                    $tfd = $complemento->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
                    $selloSAT = $tfd['SelloSAT'];
                    $selloCFD = $tfd['SelloCFD'];
                    $fechatimbrado = $tfd['FechaTimbrado'];
                    $csdEmisor = $complemento['NoCertificado'];
                    $certSat = $tfd['NoCertificadoSAT'];

                    $qrCadena = 'https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=' . $uuid . '&re=' . $RFC . '&rr=' . $rfcmanual . '&tt=' . $total_ticket . '0000';
                    $qrCode = new QrCode($qrCadena);
                    header('Content-Type: ' . $qrCode->getContentType());
                    $qrCode->writeString();
                    $ubiQr = "pdf-template/" . $uuid . "qrcode.png";
                    $ubicacion = $qrCode->writeFile($ubiQr);
                    $iva = number_format(($total_ticket['total_ticket'] / 1.16) * .16, 2);
                }
            }


            ////////////////////TERMINA CREACION DE XML ///////////////////////////////////////////
            ///////////Empieza pdf//////////////////////////////

            ob_start();
            ?>
            <!DOCTYPE html>
            <html lang="es">
            <head>
                <meta charset="utf-8">
                <link rel="stylesheet" href="pdf-template/style.css" media="all"/>
            </head>
            <body>
            <header class="clearfix">
                <div id="logo">
                    <img src="pdf-template/img/logo.png">
                </div>
                <h1>DATOS FACTURA</h1>
                <h4 align="center">DATOS EMPRESA</h4>
                <table style="text-align:center;">
                    <thead>
                    <tr>
                        <th>Razón social</th>
                        <th>RFC</th>
                        <th>Dirección</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="service"><?= $razonsocialEmp ?></td>
                        <td class="desc"><?= $RFC ?></td>
                        <td class="unit">AV INSURGENTES SUR 1863 301-B GUADALUPE INN. ALVARO OBREGON CIUDAD DE MÉXICO CP
                            01020
                        </td>
                    </tr>
                    </tbody>
                </table>
            </header>
            <h4 align="center">DATOS CLIENTE</h4>
            <table style="text-align:center;">
                <thead>
                <tr>
                    <th>Cliente</th>
                    <th>RFC</th>
                    <!-- <th>Dirección</th>-->
                    <!--<th>Codigo Postal</th>-->
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="service"><?= $razonsocial ?></td>
                    <td class="desc"><?= $rfc ?></td>
                    <!--<td class="unit"><?= $calle ?></td>-->
                    <!--<td class="qty"><?= $cp ?>< /td>-->
                </tr>
                </tbody>
            </table>
            <h4 align="center">FACTURA TIPO (I)</h4>
            <table style="text-align:center;">
                <thead>
                <tr>
                    <th>Serie-folio</th>
                    <th>Folio fiscal</th>
                    <th>No.Serie del CSD Emisor</th>
                </tr>
                </thead>
                </tr>
                <tr>
                    <td><?= $seriePark . $folioPark + 1 ?></td>
                    <td><?php echo $uuid ?></td>
                    <td><?php echo $csdEmisor ?></td>
                </tr>
                <tr>
                    <th>Fecha y Hora de emision</th>
                    <th>Fecha y Hora de certificación</th>
                    <th>No.Serie del CSD del SAT</th>
                </tr>
                <tr>
                    <td><?= $timestamp ?></td>
                    <td><?= $fechatimbrado ?></td>
                    <td><?= $certSat ?></td>

                </tr>
                </tbody>
            </table>
            <table style="text-align:center;">
                <thead>
                <tr>
                    <th>Uso CFDI</th>
                    <th>Metodo de Pago</th>
                    <th>Forma de Pago</th>
                    <th>Regimen fiscal</th>
                </tr>
                </thead>
                </tr>
                <tr>
                    <td><?= $concepto ?></td>
                    <td>PUE</td>
                    <td><?= $pago ?></td>
                    <td>General de Ley Personas Morales(601)</td>
                </tr>
                </tbody>
            </table>
            <h4 align="center">CONCEPTOS</h4>
            <main>
                <table style="text-align:center;">
                    <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Unidad</th>
                        <th>Precio Unitario</th>
                        <th>Descuento</th>
                        <th>Importe</th>
                        <th>IVA</th>
                        <th>IEPS</th>
                        <th>IMP.EST.</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="service">1</td>
                        <td class="desc">E48/Unidad de servicio</td>
                        <td class="unit"><?= number_format($total_ticket['total_ticket'] / 1.16, 2) ?></td>
                        <td class="qty"></td>
                        <td class="total"><?= $importe ?></td>
                        <td><?= $iva ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>

                <table style="text-align:center;">
                    <thead>
                    <tr>
                        <th>Total</th>
                        <th>subTotal</th>
                        <th>I.V.A. 16%</th>
                        <th>Total IVA</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="service"><?= $total_ticket ?></td>
                        <td class="desc"><?= $subtotal ?></td>
                        <td class="unit"><?= $iva ?></td>
                        <td class="qty"><?= $iva ?></td>
                        <td class="total"><?= $total_ticket ?></td>
                    </tr>
                    <!--columnas extras-->
                    <!-- <tr>
                         <td class="service">Development</td>
                         <td class="desc">Developing a Content Management System-based Website</td>
                         <td class="unit">$40.00</td>
                         <td class="qty">80</td>
                         <td class="total">$3,200.00</td>
                     </tr>
                     <tr>
                         <td class="service">SEO</td>
                         <td class="desc">Optimize the site for search engines (SEO)</td>
                         <td class="unit">$40.00</td>
                         <td class="qty">20</td>
                         <td class="total">$800.00</td>
                     </tr>
                     <tr>
                         <td class="service">Training</td>
                         <td class="desc">Initial training sessions for staff responsible for uploading web content</td>
                         <td class="unit">$40.00</td>
                         <td class="qty">4</td>
                         <td class="total">$160.00</td>
                     </tr>
                     <tr>
                         <td colspan="4">SUBTOTAL</td>
                         <td class="total">$5,200.00</td>
                     </tr>
                     <tr>
                         <td colspan="4">TAX 25%</td>
                         <td class="total">$1,300.00</td>
                     </tr>
                     <tr>
                         <td colspan="4" class="grand total">GRAND TOTAL</td>
                         <td class="grand total">$6,500.00</td>
                     </tr>-->
                    </tbody>
                </table>

                <div id="notices">
                    <div class="notice">"LA ALTERACIÓN, FALSIFICACIÓN O COMERCIALIZACIÓN ILEGAL DE ESTE DOCUMENTO ESTA
                        PENADO POR LA LEY".
                    </div>
                    <table style="text-align:center;">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Cadena original</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><img src="<?= $ubiQr ?>" style="width: 140px;"></td>
                            <td class="service"><?= $cadenaorigen ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <p>Sello SAT</p>
                <p><?= $registro_salto = wordwrap($selloSAT, 120, "\n", true); ?></p>
                <p>Sello CFD</p>
                <p><?= $registro_salto1 = wordwrap($selloCFD, 120, "\n", true); ?></p>
                <p>Estacionamiento: <?= $no_est ?></p>
                <p>"Este documento es una representación impresa de un CFDI"</p>
                </div>
            </main>

            </body>
            </html>
            <?php
            $dompdf = new Dompdf();
            $dompdf->loadHtml(ob_get_clean());
            $dompdf->setPaper('DEFAULT_PDF_PAPER_SIZE', 'portrait');
            ini_set("memory_limit", "256M");
            $dompdf->render();
//$dompdf->stream('temporal/prueba.pdf');
            $pdf = $dompdf->output();
            file_put_contents("temporal/" . $uuid . ".pdf", $pdf);

            /////////TERMINA PDF///////////////////////////////

            $folioPark1 = $folioPark + 1;
            $im = file_get_contents($pdf);
            $pdf64 = base64_encode($im);

            /*$selmax1 = "SELECT MAX(id) FROM autof_tickets";
            $consultamax1 = mysqli_query($mysqli, $selmax1);
            $fmax1 = mysqli_fetch_assoc($consultamax1);
            $idmax1 = $fmax1['MAX(id)'];*/

            $sql_auto = "INSERT INTO auto_facts (serie, tipo_doc,folio, fecha_timbrado, uuid ,subtotal_factura,iva_factura, total_factura, XML, PDF ,estatus,created_at, id_ticketAF) VALUES
    ('" . $seriePark . "','I','" . $folioPark1 . "', '" . $timestamp . "', '" . $uuid . "' ,'" . $subtotal . "','" . $iva . "','" . $total_ticket . "','" . $xml3 . "','" . $pdf64 . "' , 'timbrada','" . $timestamp . "' ,'" . $idmax . "')";
            if ($mysqli->query($sql_auto) === TRUE) {

                $sql_parks = "UPDATE parks SET folio = '" . $folioPark1 . "' WHERE  no_est = '" . $id_estacionamiento . "' ";

                if (mysqli_query($mysqli, $sql_parks)) {
                    //inicia envio de correo

                    $mail = new PHPMailer;
                    $mail->isSMTP();
                    $mail->SMTPDebug = 0;
                    $mail->Debugoutput = 'html';
                    /*$mail->Host = 'smtp.gmail.com';
                    $mail->Port = 587;
                    $mail->SMTPSecure = 'tls';
                    $mail->SMTPAuth = true;
                    $mail->Username = "ocefacturacion@gmail.com";
                    $mail->Password = "Integrador1";
                    $mail->setFrom('ocefacturacion@gmail.com', 'Central operadora de estacionamientos');*/
                    $mail->Host = 'smtp.office365.com';
                    $mail->Port = 587;
                    $mail->SMTPSecure = 'tls';
                    $mail->SMTPAuth = true;
                    $mail->Username = "facturacion-oce@central-mx.com";
                    $mail->Password = "1t3gr4d0r2020*";
                    $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
                    $mail->Subject = "Factura" . $seriePark . $folioPark;
                    $mail->Body = "<html><h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
 
<p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: $uuid, Serie: $seriePark y Folio: $folioPark. Así como su representación impresa.</p>

<p align='center;'><strong>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</strong></p>

<p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico $email a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
                    $mail->AddAddress($email);
                    $archivo = 'temporal/' . $uuid . '.xml';
                    $pdf = 'temporal/' . $uuid . '.pdf';
                    $mail->AddAttachment($archivo);
                    $mail->AddAttachment($pdf);
                    $mail->IsHTML(true);
                    $mail->Send();
                    //echo "paso";
                    unlink($archivo);
                    //echo "1";
                    unlink($pdf);
                    unlink($ubiQr);
                    //echo "2";
                    header('location:../extend/alerta?msj=Su factura fue enviada exitosamente a su correo electronico&c=menu&p=menu&t=success');
                } else {
                    header('location:../extend/alerta?msj=Ocurrio un error, intente de nuevo&c=menu&p=menu&t=error');
                }
                //echo "3";
            } else {
                header('location:../extend/alerta?msj=Ocurrio un error, intente de nuevo&c=menu&p=menu&t=error');

                //echo "fallo";
                //echo $mysqli->error;
            }
            ////////////TERmina envio de mail////////////////////////////
            //echo '<p>Se ha enviado correctamente el email a '.$mail.'!</p>';

        }//termina else de insert
        else {
            //echo "Intente de nuevo";
            //echo $mysqli->error;
            header('location:../extend/alerta?msj=Intente de nuevo&c=menu&p=menu&t=error');
        }


    }//termina else de informacion ingresada

}//termina else de row

?>

