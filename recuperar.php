<?php @session_start();//incluyendo la conexion para sesion?>
<div class="loader"></div>
<style>.loader {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('img/dash.gif') 50% 50% no-repeat #f9f9f9;
        opacity: .8
    }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">$(window).load(function () {
        $(".loader").fadeOut("slow")
    });</script>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Facturación OCE</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdb.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<style>
    html,
    body,
    header,
    .jarallax {
        height: 700px;
    }

    @media (max-width: 740px) {
        html,
        body,
        header,
        .jarallax {
            height: 100vh;
        }
    }

    @media (min-width: 800px) and (max-width: 850px) {
        html,
        body,
        header,
        .jarallax {
            height: 100vh;
        }
    }

    @media (min-width: 560px) and (max-width: 660px) {
        header .jarallax h5 {
            display: none !important;
        }
    }

    .page-footer {
        margin-top: 0px;
        padding-top: 0px;
    }

</style>

<header>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black">
        <div class="container smooth-scroll">
            <a class="navbar-brand" href="index"><img src="img/logo/logo/login-logo.png" style="width: 45px;"
                                                      href="home"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="navbar-nav mr-auto"></ul>
            </div>
        </div>
    </nav>
    <body style="background-image: url(https://c.wallhere.com/photos/0e/bc/parking_underground_marking-1213874.jpg!d); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <main>
        <br><br>
        <div class="container py-lg-5">


            <section id="clientes" class="section team-section pb-4 wow fadeIn" data-wow-delay="0.3s">
                <br>
                <p class="text-center grey-text mb-6 mx-auto w-responsive" style="font-size: 32px;"></p>

                <!-- Card -->
                <div class="card hoverable">

                    <div class="card-body">

                        <h5 class="card-header aqua-gradient white-text text-center py-4">
                            <strong>Recuperación de cuenta</strong>
                        </h5>

                        <!--Card content-->
                        <div class="card-body px-lg-5">

                            <!-- Form -->
                            <form class="text-center" style="color: #757575;" action="login/recuperacion.php"
                                  method="post" autocomplete="off">

                                <p>Ingresa tu correo electronico.</p>


                                <div class="md-form">
                                    <input type="email" id="correo" name="correo" class="form-control" required>
                                    <label for="correo">Correo</label>
                                </div>

                                <!-- Sign in button -->
                                <button class="btn btn-default btn-rounded btn-block z-depth-0 my-4 waves-effect"
                                        type="submit">Enviar
                                </button>

                            </form>
                            <!-- Form -->

                            <a href="index"><i class="fas fa-arrow-circle-left"></i> Regresar a la pantalla
                                principal</a>

                        </div>


                    </div>
                </div>


        </div>

        </section>

        </div>

    </main>
    <!-- Main Layout -->

    <!-- Footer -->
    <footer class="page-footer pt-4 mt-4 black text-center text-md-left">

        <!-- Footer Links -->
        <div class="container">
            <div class="row">

                <!-- First column -->
                <div class="col-md-6">
                    <!-- Aqui va el certificado -->
                    <p></p>
                </div>
                <!-- First column -->
                <div style="padding-left: 250px; padding-top: 50px;">
                    <h6><a href="https://www.central-mx.com/aviso-de-privacidad">Aviso de privacidad</a></h6>
                </div>

            </div>
        </div>
        <!-- Footer Links -->

        <!-- Copyright -->
        <div class="footer-copyright py-3 text-center">
            <div class="container-fluid">
                <img src="img/logo/logo/login-logo.png" style="width: 60px; padding-right: 10px;"/>
                &copy; 2019 Copyright: <a href="https://www.central-mx.com/"> Operadora Central de Estacionamientos </a>

            </div>
        </div>
        <!-- Copyright -->

    </footer>
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <script src="js/checkbox.js"></script>
    <script src="js/enviodb.js"></script>


    </body>

</html>