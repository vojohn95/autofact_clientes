//script para los check box de formulario
$(document).ready(function () {
  $("#checkfisc").click(function () {
    if (!$(this).is(":checked")) {
      $("#Check1").hide(); //muestro mediante id
    } else {
      $("#Check1").show();
    }
  });
});

$(document).ready(function () {
  $("#checkfiscauto").click(function () {
    if (!$(this).is(":checked")) {
      $("#Check2").hide(); //muestro mediante id
    } else {
      $("#Check2").show();
    }
  });
});

//codigo para select de estados
$(document).ready(function () {
  $(".mdb-select").materialSelect();
  $("#loading").hide();
});

//codigo para parralax de pension
// object-fit polyfill run
objectFitImages();

/* init Jarallax */
jarallax(document.querySelectorAll(".jarallax"));

jarallax(document.querySelectorAll(".jarallax-keep-img"), {
  keepImg: true,
});

//codigo para consulta de estacionamiento y limpieza
// version de vic
function BuscaEst() {
  var a = $("#noestacionamiento").val();

  $.ajax({
    url: "consultaestacionamiento.php",
    data: { valorBusqueda: a },
    type: "post",

    beforeSend: function () {
      $("#resultadoBusqueda").show();
      $("#manual").hide();
      $("#automatico").hide();
      $("#resultadoBusqueda").html(
        '<span>Cargando...</span><div class="progress md-progress primary-color-dark">\n' +
          '    <div class="indeterminate"></div>\n' +
          "</div>"
      );
    },
    success: function (b) {
      if (b == "Manual") {
        $("#manual").css("display", "inline");
        $("#resultadoBusqueda").hide(); //muestro mediante id
      } else if (b == "automatico") {
        $("#automatico").css("display", "inline");
        $("#resultadoBusqueda").hide(); //muestro mediante id
      } else {
        $("#manual").hide();
        $("#automatico").hide();
        $("#resultadoBusqueda").show(); //muestro mediante id
      }
      $("#resultadoBusqueda").html("");
      $("#resultadoBusqueda").html(b);
    },
  });
}

/*
$(document).ready(function () {
    //$("#resultadoBusqueda").html('<p>JQUERY VACIO</p>');
    /////////// evento keyup del buscador ///////////////////
    var load_img = false;

    $("#noestacionamiento").keyup(function () {
        var a = $(this).val();

        if (!load_img) {
            $("#loading").show();
        }
        load_img = a.length === 0 ? false : true;

        if (a.length === 0) {
            $("#loading").hide();
        }

        $.ajax({
            url: "consultaestacionamiento.php",
            data: {"valorBusqueda": a},
            type: "post",

            success: function (b) {

                if (b == "Manual") {
                    $("#noestacionamiento").on("change", function () {
                        load_img = false;
                    });
                    $("#manual").css("display", "inline");
                    $('#resultadoBusqueda').hide(); //muestro mediante id
                } else if (b == "automatico") {
                    $("#noestacionamiento").on("change", function () {
                        load_img = false;
                    });
                    $("#automatico").css("display", "inline");
                    $('#resultadoBusqueda').hide(); //muestro mediante id
                } else {
                    $("#noestacionamiento").on("change", function () {
                        load_img = false;
                    });
                    $("#manual").hide();
                    $("#automatico").hide();
                    $('#resultadoBusqueda').show(); //muestro mediante id
                }


                $("#resultadoBusqueda").html("");
                $("#resultadoBusqueda").html(b);


            }
        });
    });
    ////////////////////////////////////////////////////////
});*/

/*function buscar() {
    var textoBusqueda = $("input#noestacionamiento").val();

    if (textoBusqueda != "") {
        $.post("consultaestacionamiento.php", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#resultadoBusqueda").html(mensaje);
        });
    } else {
        $("#resultadoBusqueda").html('<p>JQUERY VACIO</p>');
    };
};*/
//form-control datepicker picker__input

//codigo para datepicker
$(".datepicker").pickadate({
  monthsFull: [
    "enero",
    "febrero",
    "marzo",
    "abril",
    "mayo",
    "junio",
    "julio",
    "agosto",
    "septiembre",
    "octubre",
    "noviembre",
    "diciembre",
  ],
  monthsShort: [
    "ene",
    "feb",
    "mar",
    "abr",
    "may",
    "jun",
    "jul",
    "ago",
    "sep",
    "oct",
    "nov",
    "dic",
  ],
  weekdaysFull: [
    "domingo",
    "lunes",
    "martes",
    "miércoles",
    "jueves",
    "viernes",
    "sábado",
  ],
  weekdaysShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
  today: "",
  clear: "Borrar",
  close: "Cerrar",
  firstDay: 1,
  format: "dd-mm-yyyy",
  formatSubmit: "dd-mm-yyyy",
});

$("#example").tooltip(options);
