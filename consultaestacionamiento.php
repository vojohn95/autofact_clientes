<?php include 'conexion/conexion.php';

error_reporting(0);
$no_est = $_POST['valorBusqueda'];
if (isset($no_est)) {
    if ($no_est == 7 || $no_est == 12 || $no_est == 66 || $no_est == 77 || $no_est == 101 || $no_est == 109 || $no_est == 222 || $no_est == 244 || $no_est == 297 || $no_est == 451 || $no_est == 502 || $no_est == 504 || $no_est == 506 || $no_est == 508 || $no_est == 546 || $no_est == 547 || $no_est == 563 || $no_est == 567 || $no_est == 832 || $no_est == 850 || $no_est == 855 || $no_est == 859 || $no_est == 891 || $no_est == 892 || $no_est == 893 || $no_est == 894 || $no_est == 895 || $no_est == 902 || $no_est == 921 || $no_est == 926) {
        echo "<i class=\"fas fa-exclamation red-text pr-3\"></i><span style='color: red;'>Lo sentimos, por el momento este estacionamiento no puede realizar facturas</span><i class=\"fas fa-exclamation red-text pr-3\"></i>";
    } elseif ($no_est == 076 || $no_est == 400 || $no_est == 413 || $no_est == 414 || $no_est == 463) {
        echo "<i class=\"fas fa-exclamation red-text pr-3\"></i><span style='color: red;'>Lo sentimos, por el momento este estacionamiento no puede realizar facturas,Por favor pase a su estacionamiento </span><i class=\"fas fa-exclamation red-text pr-3\"></i>";
    } else {
        $consulta = mysqli_query($mysqli, "SELECT nombre, Facturable, Automatico FROM parks WHERE no_est = $no_est");
        if (mysqli_num_rows($consulta) > 0) {
            while ($row = mysqli_fetch_array($consulta, MYSQLI_ASSOC)) {
                $nombre = $row['nombre'];
                $facturable = $row['Facturable'];
                $automatico = $row['Automatico'];

                switch ($facturable) {
                    case 0:
                        echo "<i class=\"fas fa-exclamation red-text pr-3\"></i><span style='color: red;'>Lo sentimos, por el momento este estacionamiento no puede realizar facturas </span><i class=\"fas fa-exclamation red-text pr-3\"></i>";
                        break;
                    case 1:
                        switch ($automatico) {
                            case 0:
                                echo "Manual";
                                break;
                            case 1:
                                echo "automatico";
                                break;
                        }
                }
            } //Termina while
        } else {
            echo "<i class=\"fas fa-exclamation red-text pr-3\"></i><span style='color: red;'>Número de estacionamiento no existe  </span><i class=\"fas fa-exclamation red-text pr-3\"></i>";
            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red;'>Si requiere solicitar su factura:</span><a href='solicitud?est=$no_est'>pulse aquí <i class=\"fas fa-taxi\"></i></a>";
        } //termina else
    }
} //Termina if
else {
    echo "No se recibio el número de estacionamiento";
}
