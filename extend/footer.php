<br>
<footer class="page-footer font-small aqua-gradient  fixed-bottom">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© <?php echo date("Y"); ?> Copyright:
        <a href="https:central-mx.com" target="_blank"> Operadora Central de Estacionamientos SAPI de C.V.  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspDesarrollado por CMR</a>
    </div>
    <!-- Copyright -->

</footer>

<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
<script src="../js/checkbox.js"></script>
<script type="text/javascript">
    //document.oncontextmenu = function(){return false;}
</script>